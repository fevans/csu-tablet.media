// check for touch device
var eventtype;
if ('ontouchstart' in window || 'onmsgesturechange' in window) {
    eventtype = window.navigator.msPointerEnabled ? "click" : "touchstart";
} else {
    eventtype = 'click';
}
// make global variables easily accessible
for (var layerGroupName in PSD) {
 window[layerGroupName] = PSD[layerGroupName];
}
// store original position
for (var layerGroupName in PSD) {
 PSD[layerGroupName].originalFrame = window[layerGroupName].frame;
}

// setup iPad frame
iPad.opacity = 0
iPad.width = 1024
iPad.height = 748
iPad.style['overflow'] = 'hidden'
iPad.animate({
	properties: {opacity:1},
	time: 100
})

// menu setup
menuScroll.superView = iPad
menuScroll.x = 0
menuScroll.y = 0
menuScroll.height = iPad.height - menuScroll.y

// setup body
body.superView = iPad
body.x = menuScroll.maxX
body.y = 0
body.width = iPad.width
body.height = iPad.height
body.class = 'body'

// header setup
header.superView = body
header.class = 'header'
header.x = 0
header.y = 0
// header content
headerContent = new View({
	y: 0,
	x: 0,
	width: 1024,
	height: 70
})
headerContent.html = 'Front page'
headerContent.class = 'headerTitle'
headerContent.style = {
	color: '#fff',
	textAlign: 'center',
	padding: '20px 0 0',
	fontWeight: 'bold',
	height: '50px'
}
headerContent.superView = header

// Reading list
Readinglist.superView = header
Readinglist.x = 860
Cancel.superView = header
Cancel.opacity = 0
moreinfo.superView = header
moreinfo.opacity = 0
moreinfoBubble.superView = iPad
moreinfoBubble.height = body.height
moreinfoBubble.width = body.width
moreinfoBubble.opacity = 0
moreinfoBubble.x = 0
moreinfoBubble.y = 0
moreinfoBubble.style = {
	background: 'rgba(0, 0, 0, .4)'
}
moreinfoBubbleText.opacity = 0
moreinfoBubbleText.x = 735
moreinfoBubbleText.y = 60

// Homepage
homepage.superView = body
homepage.y = 0
homepage.class = 'homepage framer section'
homepageContent.superView = homepageScroll

// Sections setup
sectionSetup = function(section, sectionClass) {
	section.superView = body
	section.y = 0
	section.class = sectionClass + ' framer section'
}

sectionSetup(sports, 'sports')
sportsContent.superView = sportsScroll

sectionSetup(entertainment ,'entertainment')
entertainmentContent.superView = entertainmentScroll

sectionSetup(property, 'property')
propertyContent.superView = propertyScroll

sectionSetup(technology, 'technology')
technologyContent.superView = technologyScroll

// article setup
article.superView = body
article.x = body.maxX
article.y = 0
article.class = 'article framer'
articleContent.superView = articleScroll

articleNav.superView = article
articleNav.x = 0
articleNav.y = 0
articleNav.class = 'articleNav framer'

share.superView = articleContent
share.height = articleContent.height
share.width = articleContent.width
share.x = 0
share.y = 0
share.opacity = 0
share.style = {
	background: 'rgba(0, 0, 0, .4)'
}
send.superView = share
send.x = 603
send.y = 50
send.style = {
	position: 'fixed'
}

// animation
animationCurve = "spring(500,50,800)"

// toggle menu
showMenu = function() {
  isMenuOpen = true;
  body.animate({
    properties: {
      x: menuScroll.maxX
    },
    curve: animationCurve
  });
  $('.body').children().each(function(){
    if (!$(this).hasClass('header')) {
      var style = $(this).attr('style');
      var isActive = (style.indexOf("1, 0, 0, 1, 0, 0") > 0) ? true : false;
      if (!isActive) {
        $(this).hide();
      }
    };
  });
}
hideMenu = function() {
  isMenuOpen = false;
  body.animate({
    properties: {
      x: 0
    },
    curve: animationCurve
  })
  $('.body').children().each(function(){
    $(this).show();
  });
}
menuToggle = utils.toggle(showMenu, hideMenu)

// toggle menu when click on hamburger
navigation.superView = header
navigation.class = 'navigation framer'
navigation.on(eventtype, function() {
  menuToggle()()
});

// hide menu on init
hideMenu()

// article
showArticle = function() {
	article.animate({
		properties: {
			x: 0,
			opacity: 1
		},
		time: 500,
		curve: animationCurve
	})
}

// Section swipe functions
sectionSwipeLeft = function(section, nextSection, sectionTitle) {
  Hammer(section._element).on("swipeleft", function(event) {
    if (isMenuOpen) {return};
    section.animate({
      properties: {
        x: -1024
      },
      curve: animationCurve
    })
    nextSection.animate({
      properties: {
        x: 0
      },
      curve: animationCurve
    })
    headerContent.html = sectionTitle
  });
}
sectionSwipeRight = function(section, nextSection, sectionTitle) {
  Hammer(section._element).on("swiperight", function(event) {
    if (isMenuOpen) {return};
    section.animate({
      properties: {
        x: 1024
      },
      curve: animationCurve
    })
    nextSection.animate({
      properties: {
        x: 0
      },
      curve: animationCurve
    })
    headerContent.html = sectionTitle
  });
}
// Swipe between sections
sectionSwipeLeft(homepage, sports, 'Sports')
sectionSwipeRight(sports, homepage, 'Front page')
sectionSwipeLeft(sports, entertainment, 'Entertainment')
sectionSwipeRight(entertainment, sports, 'Sports')
sectionSwipeLeft(entertainment, property, 'Property')
sectionSwipeRight(property, entertainment, 'Entertainment')
sectionSwipeLeft(property, technology, 'Technology')
sectionSwipeRight(technology,property, 'Property')


// Show article
Hammer(homepage._element).on("tap", function(event) {
	showArticle()
});
Hammer(sports._element).on("tap", function(event) {
	showArticle()
});
Hammer(entertainment._element).on("tap", function(event) {
	showArticle()
});
Hammer(property._element).on("tap", function(event) {
	showArticle()
});
// Hammer(technology._element).on("tap", function(event) {
// 	showArticle()
// });


// Article back to section
Hammer(back._element).on("tap", function(event) {
	article.animate({
		properties: {
			x: 1024,
			opacity: 0
		},
		time: 500,
		curve: animationCurve
	})
});
// Show sharing tool
Hammer(sendto._element).on("tap", function(event) {
	share.animate({
		properties: {
			opacity: 1
		},
		time: 300
	})
});
// Hide sharing tool
Hammer(share._element).on("tap", function(event) {
	share.animate({
		properties: {
			opacity: 0
		},
		time: 100
	})
});


// Show reading list nav
Hammer(Readinglist._element).on("tap", function(event) {
	moreinfo.animate({
		properties: {
			opacity: 1
		},
		time: 200
	})
	Cancel.animate({
		properties: {
			opacity: 1
		},
		time: 200
	})
	Readinglist.animate({
		properties: {
			x: Readinglist.originalFrame.x
		},
		time: 200
	})
	technologyContent.subViews.forEach(function(childView, index) {
		childView.on(eventtype, function(){
			$(this).toggleClass('read-list-item')
		});
	});
});
// Hide reading list nav
Hammer(Cancel._element).on("tap", function(event) {
	moreinfo.animate({
		properties: {
			opacity: 0
		},
		time: 200
	})
	Cancel.animate({
		properties: {
			opacity: 0
		},
		time: 200
	})
	Readinglist.animate({
		properties: {
			x: 860
		},
		time: 200
	})
	technologyContent.subViews.forEach(function(childView, index) {
		childView.removeClass('read-list-item');
	});
});
// Show more info tooltip
Hammer(moreinfo._element).on("tap", function(event) {
	moreinfoBubble.y = -746,
	moreinfoBubble.animate({
		properties: {
			opacity: 1
		},
		time: 500,
		curve: animationCurve
	})
	moreinfoBubbleText.animate({
		properties: {
			opacity: 1
		},
		time: 200,
		curve: animationCurve
	})
});
// Hide more info tooltip
Hammer(moreinfoBubble._element).on("tap", function(event) {
	moreinfoBubble.opacity = 0
	moreinfoBubble.y = 0
	moreinfoBubbleText.opacity = 0
});


// create read items
readItem1 = new View({
 x: 43,
 y: 30,
 width: 300,
 height: 364
});
readItem1.superView = technologyContent

readItem2 = new View({
 x: readItem1.maxX + 20,
 y: 33,
 width: 300,
 height: 364
});
readItem2.superView = technologyContent

readItem3 = new View({
 x: readItem2.maxX + 20,
 y: 33,
 width: 300,
 height: 364
});
readItem3.superView = technologyContent

readItem4 = new View({
 x: 43,
 y: readItem1.maxY + 30,
 width: 300,
 height: 336
});
readItem4.superView = technologyContent

readItem5 = new View({
 x: readItem4.maxX + 20,
 y: readItem1.maxY + 30,
 width: 300,
 height: 336
});
readItem5.superView = technologyContent

readItem6 = new View({
 x: readItem5.maxX + 20,
 y: readItem1.maxY + 30,
 width: 300,
 height: 336
});
readItem6.superView = technologyContent

readItem7 = new View({
 x: 43,
 y: readItem4.maxY + 30,
 width: 300,
 height: 364
});
readItem7.superView = technologyContent

readItem8 = new View({
 x: readItem7.maxX + 20,
 y: readItem5.maxY + 30,
 width: 300,
 height: 364
});
readItem8.superView = technologyContent

readItem9 = new View({
 x: readItem8.maxX + 20,
 y: readItem5.maxY + 30,
 width: 300,
 height: 364
});
readItem9.superView = technologyContent


// Swipe between articles
Hammer(article._element).on("swiperight", function(event) {
	if (isMenuOpen) {return};
	article.animate({
		properties: {
			x: 1024
		},
		time: 500,
		curve: animationCurve
	})
	utils.delay(100, function() {
  		article.x = -1024;
		showArticle()
	})
});
Hammer(article._element).on("swipeleft", function(event) {
	if (isMenuOpen) {return};
	article.animate({
		properties: {
			x: -1024
		},
		time: 500,
		curve: animationCurve
	})
	utils.delay(100, function() {
  		article.x = 1024;
		showArticle()
	})
});
