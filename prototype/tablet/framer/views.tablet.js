		window.FramerPS = window.FramerPS || {};
		window.FramerPS['tablet'] = [
	{
		"id": 85,
		"name": "iPad",
		"layerFrame": {
			"x": 0,
			"y": 0,
			"width": 6497,
			"height": 4000
		},
		"maskFrame": null,
		"image": {
			"path": "images/iPad.png",
			"frame": {
				"x": 0,
				"y": 0,
				"width": 1024,
				"height": 748
			}
		},
		"imageType": "png",
		"children": [
			
		],
		"modification": "570774916"
	},
	{
		"id": 6,
		"name": "body",
		"layerFrame": {
			"x": 0,
			"y": 0,
			"width": 6497,
			"height": 4000
		},
		"maskFrame": null,
		"image": null,
		"imageType": null,
		"children": [
			{
				"id": 198,
				"name": "article",
				"layerFrame": {
					"x": 0,
					"y": 0,
					"width": 6497,
					"height": 4000
				},
				"maskFrame": null,
				"image": null,
				"imageType": null,
				"children": [
					{
						"id": 202,
						"name": "articleScroll",
						"layerFrame": {
							"x": 1377,
							"y": 811,
							"width": 1024,
							"height": 678
						},
						"maskFrame": {
							"x": 1377,
							"y": 811,
							"width": 1024,
							"height": 678
						},
						"image": null,
						"imageType": null,
						"children": [
							{
								"id": 281,
								"name": "share",
								"layerFrame": {
									"x": 0,
									"y": 0,
									"width": 6497,
									"height": 4000
								},
								"maskFrame": null,
								"image": null,
								"imageType": null,
								"children": [
									{
										"id": 280,
										"name": "send",
										"layerFrame": {
											"x": 0,
											"y": 0,
											"width": 6497,
											"height": 4000
										},
										"maskFrame": null,
										"image": {
											"path": "images/send.png",
											"frame": {
												"x": 1980,
												"y": 869,
												"width": 402,
												"height": 150
											}
										},
										"imageType": "png",
										"children": [
											{
												"id": 279,
												"name": "Message",
												"layerFrame": {
													"x": 0,
													"y": 0,
													"width": 6497,
													"height": 4000
												},
												"maskFrame": null,
												"image": {
													"path": "images/Message.png",
													"frame": {
														"x": 2013,
														"y": 907,
														"width": 76,
														"height": 94
													}
												},
												"imageType": "png",
												"children": [
													
												],
												"modification": "737370937"
											},
											{
												"id": 274,
												"name": "Mail",
												"layerFrame": {
													"x": 0,
													"y": 0,
													"width": 6497,
													"height": 4000
												},
												"maskFrame": null,
												"image": {
													"path": "images/Mail.png",
													"frame": {
														"x": 2100,
														"y": 907,
														"width": 76,
														"height": 91
													}
												},
												"imageType": "png",
												"children": [
													
												],
												"modification": "1919922069"
											},
											{
												"id": 270,
												"name": "Twitter",
												"layerFrame": {
													"x": 0,
													"y": 0,
													"width": 6497,
													"height": 4000
												},
												"maskFrame": null,
												"image": {
													"path": "images/Twitter.png",
													"frame": {
														"x": 2187,
														"y": 907,
														"width": 76,
														"height": 91
													}
												},
												"imageType": "png",
												"children": [
													
												],
												"modification": "2018807851"
											},
											{
												"id": 266,
												"name": "Facebook",
												"layerFrame": {
													"x": 0,
													"y": 0,
													"width": 6497,
													"height": 4000
												},
												"maskFrame": null,
												"image": {
													"path": "images/Facebook.png",
													"frame": {
														"x": 2274,
														"y": 907,
														"width": 76,
														"height": 91
													}
												},
												"imageType": "png",
												"children": [
													
												],
												"modification": "1023462629"
											}
										],
										"modification": "1871977831"
									}
								],
								"modification": "492850514"
							},
							{
								"id": 212,
								"name": "articleNav",
								"layerFrame": {
									"x": 0,
									"y": 0,
									"width": 6497,
									"height": 4000
								},
								"maskFrame": null,
								"image": {
									"path": "images/articleNav.png",
									"frame": {
										"x": 1377,
										"y": 812,
										"width": 1024,
										"height": 70
									}
								},
								"imageType": "png",
								"children": [
									{
										"id": 211,
										"name": "sendto",
										"layerFrame": {
											"x": 0,
											"y": 0,
											"width": 6497,
											"height": 4000
										},
										"maskFrame": null,
										"image": {
											"path": "images/sendto.png",
											"frame": {
												"x": 2340,
												"y": 829,
												"width": 19,
												"height": 27
											}
										},
										"imageType": "png",
										"children": [
											
										],
										"modification": "411211835"
									},
									{
										"id": 208,
										"name": "back",
										"layerFrame": {
											"x": 0,
											"y": 0,
											"width": 6497,
											"height": 4000
										},
										"maskFrame": null,
										"image": {
											"path": "images/back.png",
											"frame": {
												"x": 1418,
												"y": 836,
												"width": 57,
												"height": 21
											}
										},
										"imageType": "png",
										"children": [
											
										],
										"modification": "1072536863"
									}
								],
								"modification": "1569606971"
							},
							{
								"id": 200,
								"name": "articleContent",
								"layerFrame": {
									"x": 0,
									"y": 0,
									"width": 6497,
									"height": 4000
								},
								"maskFrame": null,
								"image": {
									"path": "images/articleContent.png",
									"frame": {
										"x": 1377,
										"y": 812,
										"width": 1024,
										"height": 3188
									}
								},
								"imageType": "png",
								"children": [
									
								],
								"modification": "1554909842"
							}
						],
						"modification": "964156012"
					}
				],
				"modification": "372965710"
			},
			{
				"id": 195,
				"name": "homepage",
				"layerFrame": {
					"x": 0,
					"y": 0,
					"width": 6497,
					"height": 4000
				},
				"maskFrame": null,
				"image": null,
				"imageType": null,
				"children": [
					{
						"id": 77,
						"name": "homepageScroll",
						"layerFrame": {
							"x": 1377,
							"y": 70,
							"width": 1024,
							"height": 678
						},
						"maskFrame": {
							"x": 1377,
							"y": 70,
							"width": 1024,
							"height": 678
						},
						"image": null,
						"imageType": null,
						"children": [
							{
								"id": 79,
								"name": "homepageContent",
								"layerFrame": {
									"x": 0,
									"y": 0,
									"width": 6497,
									"height": 4000
								},
								"maskFrame": null,
								"image": {
									"path": "images/homepageContent.png",
									"frame": {
										"x": 1377,
										"y": 70,
										"width": 1024,
										"height": 2045
									}
								},
								"imageType": "png",
								"children": [
									
								],
								"modification": "288159066"
							}
						],
						"modification": "623296598"
					}
				],
				"modification": "745283285"
			},
			{
				"id": 187,
				"name": "sports",
				"layerFrame": {
					"x": 0,
					"y": 0,
					"width": 6497,
					"height": 4000
				},
				"maskFrame": null,
				"image": null,
				"imageType": null,
				"children": [
					{
						"id": 128,
						"name": "sportsScroll",
						"layerFrame": {
							"x": 2401,
							"y": 70,
							"width": 1024,
							"height": 678
						},
						"maskFrame": {
							"x": 2401,
							"y": 70,
							"width": 1024,
							"height": 678
						},
						"image": null,
						"imageType": null,
						"children": [
							{
								"id": 102,
								"name": "sportsContent",
								"layerFrame": {
									"x": 0,
									"y": 0,
									"width": 6497,
									"height": 4000
								},
								"maskFrame": null,
								"image": {
									"path": "images/sportsContent.png",
									"frame": {
										"x": 2401,
										"y": 70,
										"width": 1024,
										"height": 2090
									}
								},
								"imageType": "png",
								"children": [
									
								],
								"modification": "202360926"
							}
						],
						"modification": "163020070"
					}
				],
				"modification": "298136074"
			},
			{
				"id": 189,
				"name": "entertainment",
				"layerFrame": {
					"x": 0,
					"y": 0,
					"width": 6497,
					"height": 4000
				},
				"maskFrame": null,
				"image": null,
				"imageType": null,
				"children": [
					{
						"id": 138,
						"name": "entertainmentScroll",
						"layerFrame": {
							"x": 3425,
							"y": 70,
							"width": 1024,
							"height": 678
						},
						"maskFrame": {
							"x": 3425,
							"y": 70,
							"width": 1024,
							"height": 678
						},
						"image": null,
						"imageType": null,
						"children": [
							{
								"id": 105,
								"name": "entertainmentContent",
								"layerFrame": {
									"x": 0,
									"y": 0,
									"width": 6497,
									"height": 4000
								},
								"maskFrame": null,
								"image": {
									"path": "images/entertainmentContent.png",
									"frame": {
										"x": 3425,
										"y": 70,
										"width": 1024,
										"height": 2879
									}
								},
								"imageType": "png",
								"children": [
									
								],
								"modification": "1118749525"
							}
						],
						"modification": "1865323445"
					}
				],
				"modification": "592278522"
			},
			{
				"id": 191,
				"name": "property",
				"layerFrame": {
					"x": 0,
					"y": 0,
					"width": 6497,
					"height": 4000
				},
				"maskFrame": null,
				"image": null,
				"imageType": null,
				"children": [
					{
						"id": 142,
						"name": "propertyScroll",
						"layerFrame": {
							"x": 4449,
							"y": 70,
							"width": 1024,
							"height": 678
						},
						"maskFrame": {
							"x": 4449,
							"y": 70,
							"width": 1024,
							"height": 678
						},
						"image": null,
						"imageType": null,
						"children": [
							{
								"id": 110,
								"name": "propertyContent",
								"layerFrame": {
									"x": 0,
									"y": 0,
									"width": 6497,
									"height": 4000
								},
								"maskFrame": null,
								"image": {
									"path": "images/propertyContent.png",
									"frame": {
										"x": 4449,
										"y": 70,
										"width": 1024,
										"height": 2937
									}
								},
								"imageType": "png",
								"children": [
									
								],
								"modification": "24408067"
							}
						],
						"modification": "1185279495"
					}
				],
				"modification": "1903603167"
			},
			{
				"id": 193,
				"name": "technology",
				"layerFrame": {
					"x": 0,
					"y": 0,
					"width": 6497,
					"height": 4000
				},
				"maskFrame": null,
				"image": null,
				"imageType": null,
				"children": [
					{
						"id": 144,
						"name": "technologyScroll",
						"layerFrame": {
							"x": 5473,
							"y": 70,
							"width": 1024,
							"height": 678
						},
						"maskFrame": {
							"x": 5473,
							"y": 70,
							"width": 1024,
							"height": 678
						},
						"image": null,
						"imageType": null,
						"children": [
							{
								"id": 113,
								"name": "technologyContent",
								"layerFrame": {
									"x": 0,
									"y": 0,
									"width": 6497,
									"height": 4000
								},
								"maskFrame": null,
								"image": {
									"path": "images/technologyContent.png",
									"frame": {
										"x": 5473,
										"y": 70,
										"width": 1024,
										"height": 2946
									}
								},
								"imageType": "png",
								"children": [
									
								],
								"modification": "1565165255"
							}
						],
						"modification": "1302174335"
					}
				],
				"modification": "1107501675"
			},
			{
				"id": 60,
				"name": "header",
				"layerFrame": {
					"x": 0,
					"y": 0,
					"width": 6497,
					"height": 4000
				},
				"maskFrame": null,
				"image": {
					"path": "images/header.png",
					"frame": {
						"x": 1377,
						"y": 0,
						"width": 1024,
						"height": 70
					}
				},
				"imageType": "png",
				"children": [
					{
						"id": 308,
						"name": "Readinglist",
						"layerFrame": {
							"x": 0,
							"y": 0,
							"width": 6497,
							"height": 4000
						},
						"maskFrame": null,
						"image": {
							"path": "images/Readinglist.png",
							"frame": {
								"x": 2110,
								"y": 18,
								"width": 141,
								"height": 37
							}
						},
						"imageType": "png",
						"children": [
							
						],
						"modification": "11190105"
					},
					{
						"id": 312,
						"name": "Cancel",
						"layerFrame": {
							"x": 0,
							"y": 0,
							"width": 6497,
							"height": 4000
						},
						"maskFrame": null,
						"image": {
							"path": "images/Cancel.png",
							"frame": {
								"x": 2271,
								"y": 27,
								"width": 50,
								"height": 13
							}
						},
						"imageType": "png",
						"children": [
							
						],
						"modification": "1818007143"
					},
					{
						"id": 310,
						"name": "moreinfo",
						"layerFrame": {
							"x": 0,
							"y": 0,
							"width": 6497,
							"height": 4000
						},
						"maskFrame": null,
						"image": {
							"path": "images/moreinfo.png",
							"frame": {
								"x": 2342,
								"y": 22,
								"width": 25,
								"height": 25
							}
						},
						"imageType": "png",
						"children": [
							
						],
						"modification": "523646565"
					},
					{
						"id": 63,
						"name": "navigation",
						"layerFrame": {
							"x": 0,
							"y": 0,
							"width": 6497,
							"height": 4000
						},
						"maskFrame": null,
						"image": {
							"path": "images/navigation.png",
							"frame": {
								"x": 1395,
								"y": 16,
								"width": 42,
								"height": 38
							}
						},
						"imageType": "png",
						"children": [
							
						],
						"modification": "566038176"
					}
				],
				"modification": "1877839256"
			},
			{
				"id": 316,
				"name": "moreinfoBubble",
				"layerFrame": {
					"x": 0,
					"y": 0,
					"width": 6497,
					"height": 4000
				},
				"maskFrame": null,
				"image": null,
				"imageType": null,
				"children": [
					{
						"id": 324,
						"name": "moreinfoBubbleText",
						"layerFrame": {
							"x": 0,
							"y": 0,
							"width": 6497,
							"height": 4000
						},
						"maskFrame": null,
						"image": {
							"path": "images/moreinfoBubbleText.png",
							"frame": {
								"x": 2110,
								"y": 57,
								"width": 272,
								"height": 277
							}
						},
						"imageType": "png",
						"children": [
							
						],
						"modification": "1045400168"
					}
				],
				"modification": "449279917"
			}
		],
		"modification": "1990961534"
	},
	{
		"id": 3,
		"name": "menuScroll",
		"layerFrame": {
			"x": 1049,
			"y": 0,
			"width": 300,
			"height": 748
		},
		"maskFrame": {
			"x": 1049,
			"y": 0,
			"width": 300,
			"height": 748
		},
		"image": null,
		"imageType": null,
		"children": [
			{
				"id": 75,
				"name": "menu",
				"layerFrame": {
					"x": 0,
					"y": 0,
					"width": 6497,
					"height": 4000
				},
				"maskFrame": null,
				"image": {
					"path": "images/menu.png",
					"frame": {
						"x": 1049,
						"y": 0,
						"width": 300,
						"height": 1286
					}
				},
				"imageType": "png",
				"children": [
					
				],
				"modification": "341723381"
			}
		],
		"modification": "560213999"
	}
]