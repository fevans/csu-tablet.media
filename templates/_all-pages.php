<?php include "includes/template/variables.php" ?>

<!DOCTYPE html>
<html lang="en"> 
	<head>
		<?php include "includes/template/meta.php" ?>
		<title>All pages</title>
        <style>
                /* _all-pages.php */
            html, body {margin:0; padding:0;}
            body {font:76%/1.4 Arial,Helvetica,sans-serif;}
            a {font-weight:bold; color:#005F96; text-decoration:underline}
            .wrap {width:940px; margin:0 auto; border-color: #E2E2DD; border-style: solid; border-width: 0 1px; box-shadow: 0 0 10px rgba(50, 50, 50, 0.3); padding: 0 25px 25px;}
            h1,
            h2 {font-family:Georgia,"Times New Roman",Times,serif; font-weight:normal;}
            h1 {border-bottom:2px solid #ccc; font-size:2.3em; margin:0 0 0.8em;}
            h2 {font-size:1.7em; margin:0;}
            ul {margin:0.3em 0 0; padding-left:2em;}

            .templatesList dl {overflow:hidden;}
            .templatesList dt {clear:both;}
            .templatesList dd {float:left; width:30%; font-size:0.95em; position:relative; margin:0; padding:5px 30px; padding-right:30px;}
            .templatesList ul {margin-bottom: 0.5em;}
            .templatesList pre {font-size:0.8em;}
        </style>
	</head>
	<body class="templatesList">
        <div class="wrap">
            <h1>Tablet templates</h1>
            <dl>
                <dt><a href="article-session-times.php?publication=smh&debug=true">Movie Session Times - Working Prototype</a>
                    <ul>
                        <li><a href="article-session-times-favourite.php?publication=smh&debug=true">Template View - Favourite Cineams</a></li>
                        <li><a href="article-session-times-near.php?publication=smh&debug=true">Template View - Cinemas Near By</a></li>
                        <li><a href="article-session-times-search.php?publication=smh&debug=true">Template View - Search Cinemas</a></li>
                        <li><a href="index-content-feature-indicators.php?publication=smh&debug=true">Template - Index with Movie Indicators</a></li>
                        <li><a href="index-indicators.php?publication=smh&debug=true">Template - Landscape Index with Movie Indicators</a></li>
                    </ul>
                </dt>
                <dt><a href="index-content-feature-alt.php?publication=smh&curated=true&contentPosition=bottom-center&debug=true">Content Feature Alt Curated Index</a> (Headline text ON feature image. Default position - bottom center)
                    <ul>
                        <li><a href="index-content-feature-alt.php?publication=smh&curated=true&contentPosition=top-left&debug=true">top left</a></li>
                        <li><a href="index-content-feature-alt.php?publication=smh&curated=true&contentPosition=top-right&debug=true">top right</a></li>
                        <li><a href="index-content-feature-alt.php?publication=smh&curated=true&contentPosition=bottom-left&debug=true">bottom left</a></li>
                        <li><a href="index-content-feature-alt.php?publication=smh&curated=true&contentPosition=bottom-right&debug=true">bottom right</a></li>
                    </ul>
                </dt>
                <dt><a href="index-content-feature-alt.php?publication=smh&debug=true">Content Feature Alt Index</a> (Headline text UNDER feature image)</dt>

                <dd>Portrait: 3 Standard Items, 1 Feature Item
<pre>
-------------------------
|       |       |       |
|   1   |   2   |   3   |
|       |       |       |
|-----------------------|
|                       |
|                       |
|                       |
|           4           |
|                       |
|                       |
|                       |
-------------------------
</pre>
                </dd>
                <dd>Landscape: 1 Standard Item, 2 Brief Items, 1 Feature Item
<pre>
---------------------------------
|       |                       |
|   1   |                       |
|       |                       |
|-------|           4           |
|   2   |                       |
|-------|                       |
|   3   |                       |
---------------------------------
</pre>

                </dd>

                <dt><a href="index-content-feature.php?publication=smh&curated=true&contentPosition=bottom-center&debug=true">Content Feature Curated Index</a> (Headline text ON feature image. Default position - bottom center)
                    <ul>
                        <li><a href="index-content-feature.php?publication=smh&curated=true&contentPosition=top-left&debug=true">top left</a></li>
                        <li><a href="index-content-feature.php?publication=smh&curated=true&contentPosition=top-right&debug=true">top right</a></li>
                        <li><a href="index-content-feature.php?publication=smh&curated=true&contentPosition=bottom-left&debug=true">bottom left</a></li>
                        <li><a href="index-content-feature.php?publication=smh&curated=true&contentPosition=bottom-right&debug=true">bottom right</a></li>
                    </ul>
                </dt>
                <dt><a href="index-content-feature.php?publication=smh&debug=true">Content Feature Index</a> (Headline text UNDER feature image)</dt>
                <dd>Portrait: 1 Feature Item, 3 Standard Items
<pre>
-------------------------
|                       |
|                       |
|                       |
|           1           |
|                       |
|                       |
|                       |
|-----------------------|
|       |       |       |
|   2   |   3   |   4   |
|       |       |       |
-------------------------
</pre>
                </dd>
                <dd>Landscape: 1 Feature Item, 1 Standard Item, 2 Brief Items
<pre>
---------------------------------
|                       |       |
|                       |   2   |
|                       |       |
|           1           |-------|
|                       |   3   |
|                       |-------|
|                       |   4   |
---------------------------------
    </pre>
                </dd>

                <dt><a href="index-content-hero-alt.php?publication=smh&debug=true">Content Hero Alt Index</a></dt>
                <dd>Portrait: 1 Hero Item, 1 Wide Lead Item, 3 Standard Items
<pre>
-------------------------
|               |       |
|               |   3   |
|               |       |
|       1       |-------|
|               |       |
|               |   4   |
|               |       |
|---------------|-------|
|               |       |
|       2       |   5   |
|               |       |
-------------------------
    </pre>
                </dd>
                <dd>Landscape: 1 Hero Item , 4 Standard Items
<pre>
---------------------------------
|               |       |       |
|               |   2   |   3   |
|               |       |       |
|       1       |-------|-------|
|               |       |       |
|               |   4   |   5   |
|               |       |       |
---------------------------------
</pre>
                </dd>

                <dt><a href="index-content-hero.php?publication=smh&debug=true">Content Hero Index</a></dt>
                <dd>Portrait: 1 Hero Item, 5 Standard Items
<pre>
-------------------------
|               |       |
|               |   2   |
|               |       |
|       1       |-------|
|               |       |
|               |   3   |
|               |       |
|---------------|-------|
|       |       |       |
|   4   |   5   |   6   |
|       |       |       |
-------------------------
</pre>
                </dd>
                <dd>Landscape: 1 Hero Item , 3 Standard Items, 2 Brief Items
<pre>
---------------------------------
|               |       |       |
|               |   2   |   3   |
|               |       |       |
|       1       |-------|-------|
|               |       |   5   |
|               |   4   |-------|
|               |       |   6   |
---------------------------------
    </pre>
                </dd>

                <dt><a href="index-content-lead-tall.php?publication=smh&debug=true">Content Lead Tall Index</a></dt>
                <dd>Portrait: 1 Tall Lead Item, 7 Standard Items
<pre>
-------------------------
|       |       |       |
|       |   2   |   3   |
|       |       |       |
|   1   |-------|-------|
|       |       |       |
|       |   4   |   5   |
|       |       |       |
|-------|-------|-------|
|       |       |       |
|   6   |   7   |   8   |
|       |       |       |
-------------------------
</pre>
                </dd>
                <dd>Landscape: 1 Tall Lead Item, 5 Standard Items, 2 Brief Items
<pre>
---------------------------------
|       |       |       |       |
|       |   2   |   3   |   4   |
|       |       |       |       |
|   1   |-------|-------|-------|
|       |       |       |   7   |
|       |   5   |   6   |-------|
|       |       |       |   8   |
---------------------------------
</pre>
                </dd>
                <dt><a href="index-content-two-tall.php?publication=smh&debug=true">Content Two Tall Index</a></dt>
                <dd>Portrait: 2 Tall Items, 5 Standard Items
<pre>
-------------------------
|       |       |       |
|       |   2   |   3   |
|       |       |       |
|   1   |-------|-------|
|       |       |       |
|       |   4   |       |
|       |       |       |
|-------|-------|   5   |
|       |       |       |
|   6   |   7   |       |
|       |       |       |
-------------------------
</pre>
                </dd>
                <dd>Landscape: 2 Tall Items, 5 Standard Items
<pre>
---------------------------------
|       |       |       |       |
|       |   2   |   3   |   4   |
|       |       |       |       |
|   1   |-------|-------|-------|
|       |       |       |       |
|       |   5   |   6   |   7   |
|       |       |       |       |
---------------------------------
</pre>
                </dd>

                <dt><a href="index-content-middle-tall.php?publication=smh&debug=true">Content Middle Tall Index</a></dt>
                <dd>Portrait: 1 Tall Middle Item, 7 Standard Items
<pre>
-------------------------
|       |       |       |
|  1    |       |   3   |
|       |       |       |
|-------|   2   |-------|
|       |       |       |
|   4   |       |   5   |
|       |       |       |
|-------|-------|-------|
|       |       |       |
|   6   |   7   |   8   |
|       |       |       |
-------------------------
</pre>
                </dd>
                <dd>Landscape: 1 Tall Middle Item, 5 Standard Items, 2 Brief Items
<pre>
---------------------------------
|       |       |       |       |
|   1   |       |   3   |   4   |
|       |       |       |       |
|-------|   2   |-------|-------|
|       |       |       |   7   |
|   5   |       |   6   |-------|
|       |       |       |   8   |
---------------------------------
</pre>
                </dd>

                <dt><a href="index-content-lead-wide-alt.php?publication=smh&debug=true">Content Lead Wide Alt Index</a></dt>
                <dd>Portrait: 1 Wide Lead Item, 7 Standard Items
<pre>
-------------------------
|       |               |
|   2   |       1       |
|       |               |
|-------|---------------|
|       |       |       |
|   3   |   4   |   5   |
|       |       |       |
|-------|-------|-------|
|       |       |       |
|   6   |   7   |   8   |
|       |       |       |
-------------------------
    </pre>
                </dd>
                <dd>Landscape: 1 Wide Lead Item, 5 Standard Items, 2 Brief Items
<pre>
---------------------------------
|       |       |               |
|   3   |   2   |       1       |
|       |       |               |
|-------|-------|---------------|
|       |       |       |   7   |
|   4   |   5   |   6   |-------|
|       |       |       |   8   |
---------------------------------
</pre>
                </dd>

                <dt><a href="index-content-lead-wide.php?publication=smh&debug=true">Content Lead Wide Index</a></dt>
                <dd>Portrait: 1 Wide Lead Item, 7 Standard Items
<pre>
-------------------------
|               |       |
|       1       |   2   |
|               |       |
|---------------|-------|
|       |       |       |
|   3   |   4   |   5   |
|       |       |       |
|-------|-------|-------|
|       |       |       |
|   6   |   7   |   8   |
|       |       |       |
-------------------------
</pre>
                </dd>
                <dd>Landscape: 1 Wide Lead Item, 5 Standard Items, 2 Brief Items
<pre>
---------------------------------
|               |       |       |
|       1       |   2   |   3   |
|               |       |       |
|---------------|-------|-------|
|       |       |       |   7   |
|   4   |   5   |   6   |-------|
|       |       |       |   8   |
---------------------------------
</pre>
                </dd>

                <dt><a href="index-content-standard.php?publication=smh&debug=true">Content Standard Index</a></dt>
                <dd>Portrait: 9 Standard Items
<pre>
-------------------------
|       |       |       |
|   1   |   2   |   3   |
|       |       |       |
|-------|-------|-------|
|       |       |       |
|   4   |   5   |   6   |
|       |       |       |
|-------|-------|-------|
|       |       |       |
|   7   |   8   |   9   |
|       |       |       |
-------------------------
</pre>
                </dd>
                <dd>Landscape: 7 Standard Items, 2 Brief Items
<pre>
---------------------------------
|       |       |       |       |
|   1   |   2   |   3   |   4   |
|       |       |       |       |
|-------|-------|-------|-------|
|       |       |       |   8   |
|   5   |   6   |   7   |-------|
|       |       |       |   9   |
---------------------------------
</pre>
                </dd>

                <dt><a href="index-four-column-with-hero.php?publication=smh&debug=true">Four Column Index With Hero</a></dt>
                <dd>Portrait: 1 Hero Item, 8 Standard Items
<pre>
-------------------------
|               |       |
|               |   2   |
|               |       |
|       1       |-------|
|               |       |
|               |   3   |
|               |       |
|---------------|-------|
|       |       |       |
|   4   |   5   |   6   |
|       |       |       |
|-------|-------|-------|
|       |       |       |
|   7   |   8   |   9   |
|       |       |       |
-------------------------
</pre>
                </dd>
                <dd>Landscape: 1 Hero Item , 8 Standard Items
<pre>
---------------------------------
|               |       |       |
|               |   2   |   3   |
|               |       |       |
|       1       |-------|-------|
|               |       |       |
|               |   4   |   5   |
|               |       |       |
|---------------|-------|-------|
|       |       |       |       |
|   6   |   7   |   8   |   9   |
|       |       |       |       |
---------------------------------
</pre>
                </dd>

                <dt><a href="article-default.php?publication=smh&debug=true">Default Article Template</a></dt>
                <dd>&hellip;</dd>
                <dd>&hellip;</dd>

                <dt><a href="index-four-column.php?publication=smh&debug=true">Four Column Index</a></dt>
                <dd>Portrait: 12 Standard Items
<pre>
-------------------------
|       |       |       |
|   1   |   2   |   3   |
|       |       |       |
|-------|-------|-------|
|       |       |       |
|   4   |   5   |   6   |
|       |       |       |
|-------|-------|-------|
|       |       |       |
|   7   |   8   |   9   |
|       |       |       |
|-------|-------|-------|
|       |       |       |
|   10  |   11  |   12  |
|       |       |       |
-------------------------
</pre>
                </dd>
                <dd>Landscape: 12 Standard Items
<pre>
---------------------------------
|       |       |       |       |
|   1   |   2   |   3   |   4   |
|       |       |       |       |
|-------|-------|-------|-------|
|       |       |       |       |
|   5   |   6   |   7   |   8   |
|       |       |       |       |
|-------|-------|-------|-------|
|       |       |       |       |
|   9   |   10  |   11  |   12  |
|       |       |       |       |
---------------------------------
</pre>
                </dd>

                <dt><a href="index-content-defcon.php?publication=smh&debug=true">Defcon Index</a></dt>
                <dd>Portrait: 1 Defcon Item
<pre>
-------------------------
|                       |
|                       |
|                       |
|                       |
|                       |
|           1           |
|                       |
|                       |
|                       |
|                       |
|                       |
-------------------------
</pre>
                </dd>
                <dd>Landscape: 1 Defcon Item
<pre>
---------------------------------
|                               |
|                               |
|                               |
|               1               |
|                               |
|                               |
|                               |
---------------------------------
</pre>
                </dd>
            </dl>




        <!--Content Hero Quote Index	Index	indexTemplates/index-content-hero-quote.ftl	05/04/2012 12:50:55	Edit-->

        <!--macros	Library	macros.ftl	14/12/2011 15:20:35	Edit-->
        </div>
	</body> 
</html>