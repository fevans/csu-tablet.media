<?php include "includes/template/variables.php" ?>

<!DOCTYPE html>
<html lang="en"> 
	<head>
		<?php include "includes/template/meta.php" ?>

		<title>Standard Article</title>

        <?php include "includes/template/styles.php" ?>
	</head>
	<body class="story-content">
		<div class="wrap">
			<article>
				<header>
					<aside class="published">
						<time>${article.lastPublishedDate?string("MMMM dd, yyyy")}</time>
					</aside>
					<aside class="pagination">
						<span>article <span id="articleNum">1</span> of <span id="numOfArticles">42</span></span>
					</aside>

					<#assign relatedInfoGraphics = viewHelper.getRelatedInfoGraphics(article)!/>
					<#assign relatedVideo = viewHelper.getRelatedVideo(article)!""/>
					<#assign relatedLeadImage = viewHelper.getLeadImage(article)!""/>


                    <!--<div class="caption">-->
                        <!--<img alt="null" src="#">-->
                        <!--<cite>-->
                            <!--"You will never die of starvation on Centrelink - but to run with the great human race, to enjoy the things that make you feel proper and proud - are out reach."-->
                            <!--<em>Photo: Bloomberg</em>-->
                        <!--</cite>-->
                    <!--</div>-->

                    <!-- Add class "video" for video caption wrapper -->
                    <!--<div class="caption video">-->
                        <!--<video x-webkit-airplay="allow" controls="controls" height="410" width="729" poster="#" src="http://mediadownload.f2.com.au/flash/media/2012/07/25/3486406/iPad_vod.m3u8"></video>-->
                        <!--<cite>-->
                            <!--"You will never die of starvation on Centrelink - but to run with the great human race, to enjoy the things that make you feel proper and proud - are out reach."-->
                            <!--<em>Photo: Bloomberg</em>-->
                        <!--</cite>-->
                    <!--</div>-->

					<#assign leadAsset = "">

					<#if relatedInfoGraphics != "">
						<#assign leadAsset = "infographic">
						<#-- render related info graphics if available -->
						${viewHelper.renderInfoGraphics(relatedInfoGraphics, 729, 410)}

					<#elseif relatedVideo != "">
						<#assign leadAsset = "video">
						${viewHelper.renderVideo(article, relatedVideo, 729, 410, imageCollector, errors)}

					<#elseif viewHelper.hasPhotoGallery(article) && viewHelper.hasSuitableLeadImage(viewHelper.getPhotoGallery(article))>
						<#assign leadAsset = "gallery">
						<@renderLeadPhotoGallery gallery=viewHelper.getPhotoGallery(article)/>

					<#elseif relatedLeadImage != "">
						<#assign leadAsset = "image">
						<#-- render related image if there is no video -->
						<div class="caption">
						${viewHelper.renderImage(relatedLeadImage, imageCollector)}
							<#if viewHelper.getCaption(relatedLeadImage) != "">

								<cite>
								${viewHelper.getCaption(relatedLeadImage)}
									<#assign relatedAsset = relatedLeadImage.relatedAsset/>
									<#if relatedAsset?? && relatedAsset.byline??>
										<em>Photo: ${relatedAsset.byline}</em>
									</#if>
								</cite>
							</#if>
						</div>
					</#if>

					<h1>${article.headline}</h1>
				</header>
				<#assign hasRhsAsset = false>

				<#if leadAsset != "video" && relatedVideo != "">
					<#assign hasRhsAsset = true>
					<aside class="inlineHtml">
					${viewHelper.renderVideo(article, relatedVideo, 353, 199, imageCollector, errors)}
					</aside>
				</#if>

				<#if leadAsset != "gallery" && viewHelper.hasPhotoGallery(article)>
					<#assign hasRhsAsset = true>
					<@renderRelatedPhotoGallery gallery=viewHelper.getPhotoGallery(article)/>
				</#if>

				<#if viewHelper.hasRelatedImages(article, leadAsset == "image")>
					<#assign hasRhsAsset = true>
					<@renderRelatedImages relatedImages=viewHelper.getRelatedImages(article, leadAsset == "image")/>
				</#if>

				<#-- ad tower is displayed if there are no related assets in the rhs, ad bottom is displayed otherwise -->
				<#assign displayAdTower = !hasRhsAsset/>
				<#assign displayAdBottom = hasRhsAsset/>

				<#if displayAdTower>
					<aside id="adTower" class="ad">
					</aside>
				</#if>

				<#if article.blockquote??>
                    <!-- Add class "left || right" to align blockquote accordingly. The default position has full width without aligning -->
					<blockquote class="right">
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut tincidunt placerat mattis. Mauris at risus et lectus iaculis consequat.</p>
                        <!-- cite is optional -->
                        <cite>Consectetur Adipiscing</cite>
					</blockquote>
				</#if>

				<#if article.info??>
					<p><strong>${article.info}</strong></p>
				</#if>

				<#if article.byline??>
					<p id="byline">${article.byline}</p>
				</#if>

				<#if article.writeOff??>
					<p><strong>${article.writeOff}</strong></p>
				</#if>

				${article.body?replace("<strong />", "")}
			</article>

			<#if displayAdBottom>
				<section id="adBottom" class="ad">
				</section>
			</#if>
			<h2 class="comments">Comments</h2>
			<ul class="comments">
				<li>
					<blockquote><p></p><p>Newman said, on TV news last night, that he would be hiring more public servants. Is this little man a sycophant? Please tell us what services are being cut.</p><p></p></blockquote>
					<dl>
						<dt class="hiddenVisually">Commenter</dt>
						<dd><cite>Gerard</cite></dd>
						<dt class="hiddenVisually">Location</dt>
						<dd>Redlands</dd>
						<dt class="hiddenVisually">Date and time</dt>
						<dd class="dateTime">September 07, 2012, 10:28AM</dd>
					</dl>
					<input class="btn" type="button" value="Reply" data-commentid="55d407d0-6ddd-4ae0-88e4-d901bf976e24" data-commentername="Gerard">
					<ul>
						<li>
							<blockquote><p></p><p>What Premier Newman said about job cuts before the election. </p><p>http://www.youtube.com/watch?v=V_oKJL03XsY&amp;feature=youtu.be</p><p></p></blockquote>
							<dl>
								<dt class="hiddenVisually">Commenter</dt>
								<dd><cite>Ian</cite></dd>
								<dt class="hiddenVisually">Location</dt>
								<dd>Brisbane</dd>
								<dt class="hiddenVisually">Date and time</dt>
								<dd class="dateTime">September 07, 2012, 10:58AM</dd>
							</dl>
						</li>
						<li>
							<blockquote><p></p><p>This follows reviews finding that the so-called "audit" of Queensland's finances conducted by Peter Costello was neither independent nor an audit as such. It was only to be expected that the slothful Costello would hand in a lazy, politically-slanted effort. And for that he was paid thousands of dollars per day by a Premier crying poor.</p><p>The LNP's approach in Queensland is a warning of what an Abbott Liberal-National government would be like federally. Slash and burn, tear down services, abolish tens of thousands of jobs. With those tens of thousands of paychecks no longer available to drive consumption this has a harmful flow-on effect on small businesses that rely on healthy consumer spending to remain profitable and employing people in the private sector.</p><p>The federal Coalition have wasted the last five years, in not developing feasible policies but spending their days media-spinnig and spinning and spinning. They've treated the electorate with contempt. Lazy and arrogant, the federal Coalition deserve a few more terms in Opposition till they start behaving responsibly.</p><p></p></blockquote>
							<dl>
								<dt class="hiddenVisually">Commenter</dt>
								<dd><cite>Douglas</cite></dd>
								<dt class="hiddenVisually">Date and time</dt>
								<dd class="dateTime">September 07, 2012, 11:09AM</dd>
							</dl>
						</li>
						<li>
							<blockquote><p></p><p>Reading these comments it is no wonder this country is in such strife.  Bloated Labor government bureaucracies that taxpayers are unwilling to fund yet howl when the eventual cuts need to be made.  We will be in much better shape and thankful come 3-4 years time.</p><p></p></blockquote>
							<dl>
								<dt class="hiddenVisually">Commenter</dt>
								<dd><cite>Luke R</cite></dd>
								<dt class="hiddenVisually">Date and time</dt>
								<dd class="dateTime">September 07, 2012, 11:29AM</dd>
							</dl>
						</li>
						<li>
							<blockquote><p></p><p>Maybe more Front line staff hopefully.  Bloated middle and upper management is a major problem for all States.</p><p></p></blockquote>
							<dl>
								<dt class="hiddenVisually">Commenter</dt>
								<dd><cite>The Oracle</cite></dd>
								<dt class="hiddenVisually">Location</dt>
								<dd>Oberon</dd>
								<dt class="hiddenVisually">Date and time</dt>
								<dd class="dateTime">September 07, 2012, 11:34AM</dd>
							</dl>
						</li>
					</ul>
				</li>
				<li>
					<blockquote><p>0400 hours. Maj.General Disaster is high atop Mt Coot-tha overlooking a darkened Brisbane. Standing by are two batteries of M777 howitzers.</p><p>Admiral Costellot of the Royal Melbourne Naval Intelligence Service, submarined deep in the Brisbane River - has cleverly reworked some economic data into a grim report of QLD’s dire finances.</p><p>At the Hamilton Munitions Treasury, the LNP Army have deployed this to develop a new weapon for the Spring offensive - the Mark-1 State Budget.</p><p>Dawn approaches. Col. Unseeny, Capt. Nuckles and Brigadier Springboard arrive lugging crates of ammo marked “Budget”.  </p><p>Just then, the Forward Observer’s voice crackles over the radio..</p><p>“Shelldrake this is Sabine Raider. Fire mission over.”</p><p>“Excellent!” cries Disaster. “1 and 2 Batteries load!” </p><p>Receiving the rest of the message he bellows..</p><p>“200 000 rounds! Range 6000 metres! Fire for effect…”<br>“Fire!” </p><p>Disaster giggles gleefully watching 200 000 bundles spray out across Brisbane. A subordinate takes command. He saunters about, stopping to pick up a loose copy.</p><p>In total disbelief, he reads instead, a highly accurate rebuttal to Costellot by Air Commodore Smiggins of the Royal Qld University Aerial Reconnaissance Wing - following his brief fly-over of the state.</p><p>Revealing a situation nowhere as bad as that concocted and advising a more moderate approach to the current scorched earth policy, the rebuttal was supposedly incinerated weeks before - along with the Air Commodore. </p><p>On page 2, in bold red letters he reads “Costellot: Will Not Fly” and “Never Release To The Public”. Disaster’s face darkens murderously.</p><p>Soon after, Nuckles, Unseeny and Springboard endlessly protesting their innocence, are stuffed into the big cannons. </p><p>As the regimental band strikes up “Reach for the Stars”, the scorched and blackened trio go streaking out towards Moreton Bay, their dreadful shrieks…<a href="javascript:;" class="read-more">Read More</a></p></blockquote>
					<dl>
						<dt class="hiddenVisually">Commenter</dt>
						<dd><cite>Bombardier</cite></dd>
						<dt class="hiddenVisually">Location</dt>
						<dd>Bardon</dd>
						<dt class="hiddenVisually">Date and time</dt>
						<dd class="dateTime">September 07, 2012, 10:35AM</dd>
					</dl>
					<input class="btn" type="button" value="Reply" data-commentid="d42bf555-523f-4b31-969c-bfaf726a3045" data-commentername="Bombardier">
					<ul>
						<li>
							<blockquote><p></p><p>lol - nice work!</p><p></p></blockquote>
							<dl>
								<dt class="hiddenVisually">Commenter</dt>
								<dd><cite>cremey</cite></dd>
								<dt class="hiddenVisually">Date and time</dt>
								<dd class="dateTime">September 07, 2012, 10:49AM</dd>
							</dl>
						</li>
						<li>
							<blockquote><p>Great work. Now if only we could get an iPad into Bob Menzies in the hospital. Get well soon Bob and Bombardier keep up the reflections. The major general must be fuming... No no no noddy…<a href="javascript:;" class="read-more">Read More</a></p></blockquote>
							<dl>
								<dt class="hiddenVisually">Commenter</dt>
								<dd><cite>John Michaels</cite></dd>
								<dt class="hiddenVisually">Location</dt>
								<dd>Onshore</dd>
								<dt class="hiddenVisually">Date and time</dt>
								<dd class="dateTime">September 07, 2012, 11:11AM</dd>
							</dl>
						</li>
						<li>
							<blockquote><p></p><p>This is GOLD!</p><p></p></blockquote>
							<dl>
								<dt class="hiddenVisually">Commenter</dt>
								<dd><cite>PO'd Public Servant</cite></dd>
								<dt class="hiddenVisually">Date and time</dt>
								<dd class="dateTime">September 07, 2012, 11:26AM</dd>
							</dl>
						</li>
						<li>
							<blockquote><p></p><p>Typical Dropshort. Can clearly see the target but even with effective bracketing and clear comms from the FO you still fail to hit the target. Keep humming the Corps tune.</p><p></p></blockquote>
							<dl>
								<dt class="hiddenVisually">Commenter</dt>
								<dd><cite>Splash</cite></dd>
								<dt class="hiddenVisually">Date and time</dt>
								<dd class="dateTime">September 07, 2012, 11:27AM</dd>
							</dl>
						</li>
					</ul>
				</li>
			</ul>
			<p class="comments"><button class="btn">Post a comment</button></p>
			<p class="comments">Comments are now closed.</p>
		</div>
		<img src="resources/images/storyfooter.png">
	</body>
</html>