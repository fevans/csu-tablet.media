<?php include "includes/template/variables.php" ?>

<!DOCTYPE html>
<html lang="en"> 
	<head>
		<?php include "includes/template/meta.php" ?>

		<title>Standard Article</title>

        <?php include "includes/template/styles.php" ?>
        <link rel="stylesheet" href="resources/styles/movie-sessions.css">
        
        <script src="http://code.jquery.com/jquery-latest.min.js"></script>
        <!-- google maps api v3 -->
        <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>
        <!-- google maps infobox addition -->
        <script src="resources/scripts/infobox.js"></script>
        <!-- google maps marker label addition -->
        <script src="resources/scripts/markerwithlabel_packed.js"></script>
        <!-- custom ui interations -->
        <!-- UI NOTE - Removed for Template -->
		<!--script src="resources/scripts/sessions-ui.js"></script-->
		<!-- maps api custom implementation -->
		<script src="resources/scripts/sessions-map.js"></script>
	</head>
	<body class="story-content">
		<div class="wrap">
			<article role="main">
				<header>
					<aside class="published">
						<time>May 07, 2013 - 3:00 AM</time>
					</aside>
					<aside class="pagination">
						<span>article <span id="articleNum">1</span> of <span id="numOfArticles">42</span></span>
					</aside>
					<div class="caption">
								<img src="_demo/resources/images/session-times-hero.png" />
								<cite>
									This article is a review of the <strong>Life of Pi</strong> movie.
									<a class="cite-action scrollToSessions" href="javascript:void(0);">
										Find a session time
									</a>
								</cite>
								<div class="article-media-type trailer">
									trailer
								</div>
					</div>
					<h1>Steering the ship</h1>
				</header>
				<p id="byline">November 17 2012, 1:28AM</p>
				<h3>Ang Lee enhanced his reputation as a director who can do no wrong with his stunning adaption of Life of Pi.</h3>
				<p>Lorem ipsum dolor sit amet, inani affert populo ex mel, pri alienum corrumpit dissentias in, ei vix porro dolore eirmod. An unum viderer dissentias vix. Sit dicta fabulas periculis ea. Ex congue dicant nec, ut ocurreret consetetur has, feugiat nominati suavitate te vis.</p>
<p>Id pro iudico cotidieque, omnium luptatum sit ad, atqui disputationi conclusionemque ei his. Ad est porro omnes veritus. Stet veniam maiorum nec at. Assueverit omittantur mediocritatem sea no.</p>
<p>Sea modo ubique no. Dolorem salutandi intellegat in vis. Te nam luptatum atomorum intellegat. Nec debitis neglegentur et. Has everti impedit praesent eu. Equidem dolorum philosophia in sea, malis repudiandae interpretaris vim cu. Ut nec nemore semper.</p>
<p>Usu ex error platonem deseruisse. Pri ex accumsan delectus voluptatum, vis nulla commodo in. Munere senserit duo ne, id per saepe reprimique percipitur. Movet euismod recteque has ei, ad vix impedit iracundia expetendis. Primis phaedrum deserunt mea eu, eu cum putent numquam, ne vix inani maiestatis. Porro mollis convenire per ne, eos nonumes placerat principes id. Ullum tamquam signiferumque his an, an eleifend scripserit sea.</p>
				<section class="session-times">
					<div class="session-nav">
						<h2 class="session-heading"><span>Session times:</span> Life of Pi</h2>
						<ul class="find-cinema">
							<li><a href="javascript:void(0);" class="fav">Favourite Cinemas</a><span class="arrow-down"></span></li>
							<li class="selected"><a href="javascript:void(0);" class="near">Cinemas near by</a><span class="arrow-down"></span></li>
							<li><a href="javascript:void(0);" class="search">Search Cinemas</a><span class="arrow-down"></span></li>
						</ul>
					</div>
					<div class="result near">
						<div>
							<div class="lservices">
								<label>Allow location service</label>
								<div class="switch on">
									<span class="thumb"></span>
									<input type="checkbox" />
								</div>
								<!-- UI NOTE - Show/hide for location services disabled/enabled -->
								<p class="hide">Turn on location services to find Cinemas close to you.</p>
								<!-- END -->
							</div>
							<h3><span>3</span> Cinemas near by</h3>
							<div class="cinema-map" id="cinema-map"></div>
						</div>
						<div>
							<div class="cinema-info">
								<h3><span class="marker">A</span> <span class="cinema-title">Reading Cinemas</span></h3>
								<p class="lead">30 Greenfield Parade Bankstown 2000 NSW <button class="expand"><span class="less hide">less</span><span class="more">more</span></button></p>
								<div class="expand-info hide">
									<p>Phone: <span><a href="tel:02 9796 48888">(02) 9796 48888</a></span></p>
									<p>Hours: <span>Thursday hours 9:30 am-9:45 pm</span></p>
									<p>Transit: <span>Centro Bankstown, Stand 9</span></p>
									<p>All cinemas have disabled access and toilets</p>
								</div>
								<button class="favourite"><span class="add">Add to Favourites</span><span class="remove hide">Remove</span></button>
							</div>
							<ul class="cinema-day" role="navigation">
								<li>
									<a href="javascript:void(0);" class="selected">Today</a>
								</li>
								<li>
									<a href="javascript:void(0);">Tomorrow</a>
								</li>
								<li>
									<a href="javascript:void(0);">Thur 30</a>
								</li>
								<li>
									<a href="javascript:void(0);">Fri 31</a>
								</li>
								<li>
									<a href="javascript:void(0);">Mon 1</a>
								</li>
								<li>
									<a href="javascript:void(0);">Tues 2</a>
								</li>
								<li>
									<a href="javascript:void(0);">Wed 3</a>
								</li>
							</ul>
							<div class="session-info">
								<ul class="cinema-time">
									<li><div href="javascript:void(0);" class="tooltip">
											10:00am 
											<span class="hide">
												<p>Ends around 11:53am </p>
												<!-- REMOVED 08/05/13 -->
												<!--a class="cta" href="#">book tickets</a> 
												<p>Share</p>
												<ul>
													<li><a href="#"><img src="resources/images/session-fb.png" width="45" height="45" /></a></li>
													<li><a href="#"><img src="resources/images/session-tw.png" width="45" height="45" /></a></li>
													<li><a href="#"><img src="resources/images/session-go.png" width="45" height="45" /></a></li>
												</ul-->
											</span>
										</div>
									</li>
									<li><div href="javascript:void(0);" class="tooltip">
											11:00am 
											<span class="hide">
												<p>Ends around 11:53am </p>
											</span>
										</div>
									</li>
									<li><div href="javascript:void(0);" class="tooltip">
											12:00pm 
											<span class="hide">
												<p>Ends around 11:53am </p>
											</span>
										</div>
									</li>
									<li><div href="javascript:void(0);" class="tooltip">
											1:00pm 
											<span class="hide">
												<p>Ends around 11:53am </p>
											</span>
										</div>
									</li>
									<li><div href="javascript:void(0);" class="tooltip">
											2:00pm
											<span class="hide">
												<p>Ends around 11:53am </p>
											</span>
										</div>
									</li>
									<li><div href="javascript:void(0);" class="tooltip">
											3:00pm  
											<span class="hide">
												<p>Ends around 11:53am </p>
											</span>
										</div>
									</li>
									<li><div href="javascript:void(0);" class="tooltip">
											4:00pm
											<span class="hide">
												<p>Ends around 11:53am </p>
											</span>
										</div>
									</li>
									<li><div href="javascript:void(0);" class="tooltip">
											5:00pm
											<span class="hide">
												<p>Ends around 11:53am </p>
											</span>
										</div>
									</li>
									<li><div href="javascript:void(0);" class="tooltip">
											6:00pm  
											<span class="hide">
												<p>Ends around 11:53am </p>
											</span>
										</div>
									</li>
								</ul>
								<!-- REMOVED 08/05/13 -->
								<!--div>
									<a class="cta" href="#">book tickets</a>
								</div-->
							</div>
						</div>
						<div>
							<div class="cinema-info">
								<h3><span class="marker">A</span> <span class="cinema-title">Reading Cinemas</span></h3>
								<p class="lead">30 Greenfield Parade Bankstown 2000 NSW <button class="expand"><span class="less">less</span><span class="more hide">more</span></button></p>
								<div class="expand-info">
									<p>Phone: <span><a href="tel:02 9796 48888">(02) 9796 48888</a></span></p>
									<p>Hours: <span>Thursday hours 9:30 am-9:45 pm</span></p>
									<p>Transit: <span>Centro Bankstown, Stand 9</span></p>
									<p>All cinemas have disabled access and toilets</p>
								</div>
								<button class="favourite"><span class="add hide">Add to Favourites</span><span class="remove">Remove</span></button>
							</div>
							<ul class="cinema-day" role="navigation">
								<li>
									<a href="javascript:void(0);">Today</a>
								</li>
								<li>
									<a href="javascript:void(0);">Tomorrow</a>
								</li>
								<li>
									<a href="javascript:void(0);">Thur 30</a>
								</li>
								<li>
									<a href="javascript:void(0);">Fri 31</a>
								</li>
								<li>
									<a href="javascript:void(0);" class="selected">Mon 1</a>
								</li>
								<li>
									<a href="javascript:void(0);">Tues 2</a>
								</li>
								<li>
									<a href="javascript:void(0);">Wed 3</a>
								</li>
							</ul>
							<div class="session-info">
								<ul class="cinema-time">
									<li><div href="javascript:void(0);" class="tooltip">
											10:00am 
											<span>
												<p>Ends around 11:53am </p>
											</span>
										</div>
									</li>
									<li><div href="javascript:void(0);" class="tooltip">
											11:00am 
											<span class="hide">
												<p>Ends around 11:53am </p>
											</span>
										</div>
									</li>
									<li><div href="javascript:void(0);" class="tooltip">
											12:00pm 
											<span class="hide">
												<p>Ends around 11:53am </p>
											</span>
										</div>
									</li>
									<li><div href="javascript:void(0);" class="tooltip">
											1:00pm 
											<span class="hide">
												<p>Ends around 11:53am </p>
											</span>
										</div>
									</li>
									<li><div href="javascript:void(0);" class="tooltip">
											2:00pm
											<span class="hide">
												<p>Ends around 11:53am </p>
											</span>
										</div>
									</li>
									<li><div href="javascript:void(0);" class="tooltip">
											3:00pm  
											<span class="hide">
												<p>Ends around 11:53am </p>
											</span>
										</div>
									</li>
								</ul>
							</div>
							
						</div>
					</div>
					<div class="footer">
						<a href="javascript:void(0);"><h2>Search for other movies</h2></a>
					</div>
				</section>
			</article>
		</div>
		<img src="resources/images/storyfooter.png">
	</body>
</html>