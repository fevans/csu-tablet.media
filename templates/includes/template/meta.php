<meta charset="utf-8">

<!-- Fix viewport scale and initial width to native device screen -->
<meta name="viewport" content="initial-scale=1.0, maximum-scale=1.0, user-scalable=no, width=device-width">

<meta name="description" content="[insert SEO description]">
<meta name="keywords" content="[insert SEO keywords]">