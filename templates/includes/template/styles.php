<link rel="stylesheet" href="resources/styles/base.css">

<?php if ($news) { ?>
	<link rel="stylesheet" href="resources/styles/news.css">
<?php } ?>

<?php if ($publication) { ?>
	<link rel="stylesheet" href="resources/styles/<?php echo $publication ?>.css">
<?php } ?>

<?php if ($demo) { ?>
	<!-- Demo styles used for development purposes only - remove for production -->
	<link rel="stylesheet" href="_demo/resources/styles/base.css">

	<?php if ($news) { ?>
		<link rel="stylesheet" href="_demo/resources/styles/news.css">
	<?php } ?>

	<?php if ($publication) { ?>
		<link rel="stylesheet" href="_demo/resources/styles/<?php echo $publication ?>.css">
	<?php } ?>
<?php } ?>

<?php if ($debug) { ?>
	<!-- Debug styles used for testing purposes only - remove for production -->
	<link rel="stylesheet" href="_debug/resources/styles/base.css">
<?php } ?>