<?php
$demo = true;
$publication = (isset($_GET['publication'])) ? $_GET['publication'] : false;
$news = ($publication == 'smh' or $publication == 'age' or $publication == 'ct');
$debug = (isset($_GET['debug']) and $_GET['debug'] == 'true');
$curated = (isset($_GET['curated']) and $_GET['curated'] == 'true');
$contentPosition = (isset($_GET['contentPosition']) and $_GET['contentPosition']) ? $_GET['contentPosition'] : 'bottom-center';
?>