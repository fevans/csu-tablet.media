<?php include "includes/template/variables.php" ?>

<!DOCTYPE html>
<html lang="en"> 
	<head>
		<?php include "includes/template/meta.php" ?>
		
		<title>Defcon Content Index</title>
		
		<?php include "includes/template/styles.php" ?>
	</head>
	<body class="index index-content index-content-cover">
		
		<!--
		Portrait: 1 Defcon Item
		-------------------------
		|                       |
		|                       |
		|                       |
		|                       |
		|                       |
		|           1           |
		|                       |
		|                       |
		|                       |
		|                       |
		|                       |
		-------------------------
		
		Landscape: 1 Defcon Item
		---------------------------------
		|                               |
		|                               |
		|                               |
		|               1               |
		|                               |
		|                               |
		|                               |
		---------------------------------
		-->
		
		
		<!-- Feature Item (Portrait & Landscape) -->
		<!-- START Component: Feature Story (Image Media) -->
		<article class="story story-cover-portrait hidden-landscape">
			<a href="javascript:;">
				<!-- Story Media is optional -->
				<div class="media curated">
					<div class="group">
						<img src="_demo/resources/images/defcon-portrait-placeholder.png" width="768" height="948" alt="Lorem ipsum">
					</div>
				</div>
			</a>
		</article>

		<article class="story story-cover-landscape hidden-portrait">
			<a href="javascript:;">
				<!-- Story Media is optional -->
				<div class="media curated">
					<div class="group">
						<img src="_demo/resources/images/defcon-landscape-placeholder.png" width="1024" height="684" alt="Lorem ipsum">
					</div>
				</div>
			</a>
		</article>
		
	</body> 
</html>