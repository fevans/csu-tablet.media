<?php include "includes/template/variables.php" ?>

<!DOCTYPE html>
<html lang="en"> 
	<head>
		<?php include "includes/template/meta.php" ?>
		
		<title>Alternate Hero Content Index</title>
		
		<?php include "includes/template/styles.php" ?>
	</head>
	<body class="index index-content index-content-hero-alt">
		
		<!--
		Portrait: 1 Hero Item, 1 Wide Lead Item, 3 Standard Items
		-------------------------
		|               |       |
		|               |   3   |
		|               |       |
		|       1       |-------|
		|               |       |
		|               |   4   |
		|               |       |
		|---------------|-------|
		|               |       |
		|       2       |   5   |
		|               |       |
		-------------------------
		
		Landscape: 1 Hero Item , 4 Standard Items
		---------------------------------
		|               |       |       |
		|               |   2   |   3   |
		|               |       |       |
		|       1       |-------|-------|
		|               |       |       |
		|               |   4   |   5   |
		|               |       |       |
		---------------------------------
		-->
		
		
		<!-- Hero Item (Portrait & Landscape) -->
		<!-- START Component: Hero Story (Image Media) -->
		<article class="story story-hero">
			
			<!-- Story Link is optional -->
			<a href="javascript:;">
				
				<!-- Story Media is optional -->
				<div class="media">
					
					<div class="group">
						
						<!-- Story Image is required -->
						<img src="_demo/resources/images/hero-placeholder.png" width="479" height="471" alt="Lorem ipsum">
						
					</div>
					
				</div>
				
				<!-- Story Content is required -->
				<div class="content">
					
					<!-- Story Section is optional -->
					<h4>Lorem ipsum</h4>
					
					<div class="group">
						
						<!-- Story Indicators are optional -->
						<!-- START Component: Indicators -->
						<div class="indicators">
							
							<!-- Indicators Heading for accessibility -->
							<h5>Additional content available:</h5>
							
							<!-- Indicators List is required -->
							<ul>
								<!-- All Indicators are optional -->
								<li class="gallery">Gallery available</li>
								<li class="video">Video availble</li>
								<li class="comments">Comments accepted</li>
								<!-- Further Indicators can be added -->
							</ul>
							
						</div>
						<!-- END Component: Indicators -->
						
						<!-- Story Headline is required -->
						<h3>Lorem ipsum dolor sit amet</h3>
						
						<!-- Story Write-off is required -->
						<p><time datetime="[date/time]">[timestamp]</time> Lorem ipsum dolor sit amet, consectetur adipiscing elit. In congue fermentum aliquet. Aliquam eget ante quam, eu commodo orci. Donec varius laoreet sodales. Cras ut magna eu turpis adipiscing aliquam. Donec sed dolor et dui varius auctor. Lorem ipsum dolor sit amet, consectetur adipiscing elit. In congue fermentum aliquet. Aliquam eget ante quam, eu commodo orci. Donec varius laoreet sodales. Cras ut magna eu turpis adipiscing aliquam. Donec sed dolor et dui varius auctor.</p>
						
					</div>
					
				</div>
				
			</a>
			
		</article>
		<!-- END Component: Hero Story (Image Media) -->
		
		
		<!-- Wide Lead Item (Portrait) / Hidden Item (Landscape) -->
		<!-- START Component: Wide Lead (Image Media) -->
		<article class="story story-lead-wide-portrait hidden-landscape">
			
			<!-- Story Link is optional -->
			<a href="javascript:;">
				
				<!-- Story Media is optional -->
				<div class="media">
					
					<div class="group">
						
						<!-- Story Image is required -->
						<img src="_demo/resources/images/lead-wide-placeholder.png" width="479" height="297" alt="Lorem ipsum">
						
					</div>
					
				</div>
				
				<!-- Story Content is required -->
				<div class="content">
					
					<!-- Story Section is optional -->
					<h4>Lorem ipsum</h4>
					
					<div class="group">
						
						<!-- Story Indicators are optional -->
						<!-- START Component: Indicators -->
						<div class="indicators">
							
							<!-- Indicators Heading for accessibility -->
							<h5>Additional content available:</h5>
							
							<!-- Indicators List is required -->
							<ul>
								<!-- All Indicators are optional -->
								<li class="gallery">Gallery available</li>
								<li class="video">Video availble</li>
								<li class="comments">Comments accepted</li>
								<!-- Further Indicators can be added -->
							</ul>
							
						</div>
						<!-- END Component: Indicators -->
						
						<!-- Story Headline is required -->
						<h3>Lorem ipsum dolor sit amet</h3>
						
						<!-- Story Write-off is required -->
						<p><time datetime="[date/time]">[timestamp]</time> Lorem ipsum dolor sit amet, consectetur adipiscing elit. In congue fermentum aliquet. Aliquam eget ante quam, eu commodo orci. Donec varius laoreet sodales. Cras ut magna eu turpis adipiscing aliquam. Donec sed dolor et dui varius auctor.</p>
						
					</div>
					
				</div>
				
			</a>
			
		</article>
		<!-- END Component: Wide Lead Story (Image Media) -->
		
		
		<!-- Hidden Item (Portrait) / Standard Item (Landscape) -->
		<!-- START Component: Standard Story (Image Media) -->
		<article class="story story-standard-landscape hidden-portrait">
			
			<!-- Story Link is optional -->
			<a href="javascript:;">
				
				<!-- Story Media is optional -->
				<div class="media">
					
					<div class="group">
						
						<!-- Story Image is required -->
						<img src="_demo/resources/images/standard-placeholder.png" width="229" height="153" alt="Lorem ipsum">
						
					</div>
					
				</div>
				
				<!-- Story Content is required -->
				<div class="content">
					
					<!-- Story Section is optional -->
					<h4>Lorem ipsum</h4>
					
					<div class="group">
						
						<!-- Story Indicators are optional -->
						<!-- START Component: Indicators -->
						<div class="indicators">
							
							<!-- Indicators Heading for accessibility -->
							<h5>Additional content available:</h5>
							
							<!-- Indicators List is required -->
							<ul>
								<!-- All Indicators are optional -->
								<li class="gallery">Gallery available</li>
								<li class="video">Video availble</li>
								<li class="comments">Comments accepted</li>
								<!-- Further Indicators can be added -->
							</ul>
							
						</div>
						<!-- END Component: Indicators -->
						
						<!-- Story Headline is required -->
						<h3>Lorem ipsum dolor sit amet</h3>
						
						<!-- Story Write-off is required -->
						<p><time datetime="[date/time]">[timestamp]</time> Lorem ipsum dolor sit amet, consectetur adipiscing elit. In congue fermentum aliquet. Aliquam eget ante quam, eu commodo orci. Donec varius laoreet sodales. Cras ut magna eu turpis adipiscing aliquam. Donec sed dolor et dui varius auctor.</p>
						
					</div>
					
				</div>
				
			</a>
			
		</article>
		<!-- END Component: Standard Story (Image Media) -->
		
		
		<!-- Standard Item (Portrait & Landscape) -->
		<!-- START Component: Standard Story (Image Media) -->
		<article class="story story-standard">
			
			<!-- Story Link is optional -->
			<a href="javascript:;">
				
				<!-- Story Media is optional -->
				<div class="media">
					
					<div class="group">
						
						<!-- Story Image is required -->
						<img src="_demo/resources/images/standard-placeholder.png" width="229" height="153" alt="Lorem ipsum">
						
					</div>
					
				</div>
				
				<!-- Story Content is required -->
				<div class="content">
					
					<!-- Story Section is optional -->
					<h4>Lorem ipsum</h4>
					
					<div class="group">
						
						<!-- Story Indicators are optional -->
						<!-- START Component: Indicators -->
						<div class="indicators">
							
							<!-- Indicators Heading for accessibility -->
							<h5>Additional content available:</h5>
							
							<!-- Indicators List is required -->
							<ul>
								<!-- All Indicators are optional -->
								<li class="gallery">Gallery available</li>
								<li class="video">Video availble</li>
								<li class="comments">Comments accepted</li>
								<!-- Further Indicators can be added -->
							</ul>
							
						</div>
						<!-- END Component: Indicators -->
						
						<!-- Story Headline is required -->
						<h3>Lorem ipsum dolor sit amet</h3>
						
						<!-- Story Write-off is required -->
						<p><time datetime="[date/time]">[timestamp]</time> Lorem ipsum dolor sit amet, consectetur adipiscing elit. In congue fermentum aliquet. Aliquam eget ante quam, eu commodo orci. Donec varius laoreet sodales. Cras ut magna eu turpis adipiscing aliquam. Donec sed dolor et dui varius auctor.</p>
						
					</div>
					
				</div>
				
			</a>
			
		</article>
		<!-- END Component: Standard Story (Image Media) -->
		
		
		<!-- Standard Item (Portrait & Landscape) -->
		<!-- START Component: Standard Story (Image Media) -->
		<article class="story story-standard">
			
			<!-- Story Link is optional -->
			<a href="javascript:;">
				
				<!-- Story Media is optional -->
				<div class="media">
					
					<div class="group">
						
						<!-- Story Image is required -->
						<img src="_demo/resources/images/standard-placeholder.png" width="229" height="153" alt="Lorem ipsum">
						
					</div>
					
				</div>
				
				<!-- Story Content is required -->
				<div class="content">
					
					<!-- Story Section is optional -->
					<h4>Lorem ipsum</h4>
					
					<div class="group">
						
						<!-- Story Indicators are optional -->
						<!-- START Component: Indicators -->
						<div class="indicators">
							
							<!-- Indicators Heading for accessibility -->
							<h5>Additional content available:</h5>
							
							<!-- Indicators List is required -->
							<ul>
								<!-- All Indicators are optional -->
								<li class="gallery">Gallery available</li>
								<li class="video">Video availble</li>
								<li class="comments">Comments accepted</li>
								<!-- Further Indicators can be added -->
							</ul>
							
						</div>
						<!-- END Component: Indicators -->
						
						<!-- Story Headline is required -->
						<h3>Lorem ipsum dolor sit amet</h3>
						
						<!-- Story Write-off is required -->
						<p><time datetime="[date/time]">[timestamp]</time> Lorem ipsum dolor sit amet, consectetur adipiscing elit. In congue fermentum aliquet. Aliquam eget ante quam, eu commodo orci. Donec varius laoreet sodales. Cras ut magna eu turpis adipiscing aliquam. Donec sed dolor et dui varius auctor.</p>
						
					</div>
					
				</div>
				
			</a>
			
		</article>
		<!-- END Component: Standard Story (Image Media) -->
		
		
		<!-- Standard Item (Portrait & Landscape) -->
		<!-- START Component: Standard Story (Image Media) -->
		<article class="story story-standard">
			
			<!-- Story Link is optional -->
			<a href="javascript:;">
				
				<!-- Story Media is optional -->
				<div class="media">
					
					<div class="group">
						
						<!-- Story Image is required -->
						<img src="_demo/resources/images/standard-placeholder.png" width="229" height="153" alt="Lorem ipsum">
						
					</div>
					
				</div>
				
				<!-- Story Content is required -->
				<div class="content">
					
					<!-- Story Section is optional -->
					<h4>Lorem ipsum</h4>
					
					<div class="group">
						
						<!-- Story Indicators are optional -->
						<!-- START Component: Indicators -->
						<div class="indicators">
							
							<!-- Indicators Heading for accessibility -->
							<h5>Additional content available:</h5>
							
							<!-- Indicators List is required -->
							<ul>
								<!-- All Indicators are optional -->
								<li class="gallery">Gallery available</li>
								<li class="video">Video availble</li>
								<li class="comments">Comments accepted</li>
								<!-- Further Indicators can be added -->
							</ul>
							
						</div>
						<!-- END Component: Indicators -->
						
						<!-- Story Headline is required -->
						<h3>Lorem ipsum dolor sit amet</h3>
						
						<!-- Story Write-off is required -->
						<p><time datetime="[date/time]">[timestamp]</time> Lorem ipsum dolor sit amet, consectetur adipiscing elit. In congue fermentum aliquet. Aliquam eget ante quam, eu commodo orci. Donec varius laoreet sodales. Cras ut magna eu turpis adipiscing aliquam. Donec sed dolor et dui varius auctor.</p>
						
					</div>
					
				</div>
				
			</a>
			
		</article>
		<!-- END Component: Standard Story (Image Media) -->
		
	</body> 
</html>