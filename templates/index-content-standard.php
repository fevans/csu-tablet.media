<?php include "includes/template/variables.php" ?>

<!DOCTYPE html>
<html lang="en"> 
	<head>
		<?php include "includes/template/meta.php" ?>
		
		<title>Standard Content Index</title>
		
		<?php include "includes/template/styles.php" ?>
	</head>
	<body class="index index-content index-content-standard">
		
		<!--
		Portrait: 9 Standard Items
		-------------------------
		|       |       |       |
		|   1   |   2   |   3   |
		|       |       |       |
		|-------|-------|-------|
		|       |       |       |
		|   4   |   5   |   6   |
		|       |       |       |
		|-------|-------|-------|
		|       |       |       |
		|   7   |   8   |   9   |
		|       |       |       |
		-------------------------
		
		Landscape: 7 Standard Items, 2 Brief Items
		---------------------------------
		|       |       |       |       |
		|   1   |   2   |   3   |   4   |
		|       |       |       |       |
		|-------|-------|-------|-------|
		|       |       |       |   8   |
		|   5   |   6   |   7   |-------|
		|       |       |       |   9   |
		---------------------------------
		-->
		
		
		<!-- Standard Item (Portrait & Landscape) -->
		<!-- START Component: Standard Story (Image Media) -->
		<article class="story story-standard">
			
			<!-- Story Link is optional -->
			<a href="javascript:;">
				
				<!-- Story Media is optional -->
				<div class="media">
					
					<div class="group">
						
						<!-- Story Image is required -->
						<img src="_demo/resources/images/standard-placeholder.png" width="229" height="153" alt="Lorem ipsum">
						
					</div>
					
				</div>
				
				<!-- Story Content is required -->
				<div class="content">
					
					<!-- Story Section is optional -->
					<h4>Lorem ipsum</h4>
					
					<div class="group">
						
						<!-- Story Indicators are optional -->
						<!-- START Component: Indicators -->
						<div class="indicators">
							
							<!-- Indicators Heading for accessibility -->
							<h5>Additional content available:</h5>
							
							<!-- Indicators List is required -->
							<ul>
								<!-- All Indicators are optional -->
								<li class="gallery">Gallery available</li>
								<li class="video">Video availble</li>
								<li class="comments">Comments accepted</li>
								<!-- Further Indicators can be added -->
							</ul>
							
						</div>
						<!-- END Component: Indicators -->
						
						<!-- Story Headline is required -->
						<h3>Lorem ipsum dolor sit amet</h3>
						
						<!-- Story Write-off is required -->
						<p><time datetime="[date/time]">[timestamp]</time> Lorem ipsum dolor sit amet, consectetur adipiscing elit. In congue fermentum aliquet. Aliquam eget ante quam, eu commodo orci. Donec varius laoreet sodales. Cras ut magna eu turpis adipiscing aliquam. Donec sed dolor et dui varius auctor.</p>
						
					</div>
					
				</div>
				
			</a>
			
		</article>
		<!-- END Component: Standard Story (Image Media) -->
		
		
		<!-- Standard Item (Portrait & Landscape) -->
		<!-- START Component: Standard Story (Image Media) -->
		<article class="story story-standard">
			
			<!-- Story Link is optional -->
			<a href="javascript:;">
				
				<!-- Story Media is optional -->
				<div class="media">
					
					<div class="group">
						
						<!-- Story Image is required -->
						<img src="_demo/resources/images/standard-placeholder.png" width="229" height="153" alt="Lorem ipsum">
						
					</div>
					
				</div>
				
				<!-- Story Content is required -->
				<div class="content">
					
					<!-- Story Section is optional -->
					<h4>Lorem ipsum</h4>
					
					<div class="group">
						
						<!-- Story Indicators are optional -->
						<!-- START Component: Indicators -->
						<div class="indicators">
							
							<!-- Indicators Heading for accessibility -->
							<h5>Additional content available:</h5>
							
							<!-- Indicators List is required -->
							<ul>
								<!-- All Indicators are optional -->
								<li class="gallery">Gallery available</li>
								<li class="video">Video availble</li>
								<li class="comments">Comments accepted</li>
								<!-- Further Indicators can be added -->
							</ul>
							
						</div>
						<!-- END Component: Indicators -->
						
						<!-- Story Headline is required -->
						<h3>Lorem ipsum dolor sit amet</h3>
						
						<!-- Story Write-off is required -->
						<p><time datetime="[date/time]">[timestamp]</time> Lorem ipsum dolor sit amet, consectetur adipiscing elit. In congue fermentum aliquet. Aliquam eget ante quam, eu commodo orci. Donec varius laoreet sodales. Cras ut magna eu turpis adipiscing aliquam. Donec sed dolor et dui varius auctor.</p>
						
					</div>
					
				</div>
				
			</a>
			
		</article>
		<!-- END Component: Standard Story (Image Media) -->
		
		
		<!-- Standard Item (Portrait & Landscape) -->
		<!-- START Component: Standard Story (Image Media) -->
		<article class="story story-standard">
			
			<!-- Story Link is optional -->
			<a href="javascript:;">
				
				<!-- Story Media is optional -->
				<div class="media">
					
					<div class="group">
						
						<!-- Story Image is required -->
						<img src="_demo/resources/images/standard-placeholder.png" width="229" height="153" alt="Lorem ipsum">
						
					</div>
					
				</div>
				
				<!-- Story Content is required -->
				<div class="content">
					
					<!-- Story Section is optional -->
					<h4>Lorem ipsum</h4>
					
					<div class="group">
						
						<!-- Story Indicators are optional -->
						<!-- START Component: Indicators -->
						<div class="indicators">
							
							<!-- Indicators Heading for accessibility -->
							<h5>Additional content available:</h5>
							
							<!-- Indicators List is required -->
							<ul>
								<!-- All Indicators are optional -->
								<li class="gallery">Gallery available</li>
								<li class="video">Video availble</li>
								<li class="comments">Comments accepted</li>
								<!-- Further Indicators can be added -->
							</ul>
							
						</div>
						<!-- END Component: Indicators -->
						
						<!-- Story Headline is required -->
						<h3>Lorem ipsum dolor sit amet</h3>
						
						<!-- Story Write-off is required -->
						<p><time datetime="[date/time]">[timestamp]</time> Lorem ipsum dolor sit amet, consectetur adipiscing elit. In congue fermentum aliquet. Aliquam eget ante quam, eu commodo orci. Donec varius laoreet sodales. Cras ut magna eu turpis adipiscing aliquam. Donec sed dolor et dui varius auctor.</p>
						
					</div>
					
				</div>
				
			</a>
			
		</article>
		<!-- END Component: Standard Story (Image Media) -->
		
		
		<!-- Standard Item (Portrait & Landscape) -->
		<!-- START Component: Standard Story (Image Media) -->
		<article class="story story-standard">
			
			<!-- Story Link is optional -->
			<a href="javascript:;">
				
				<!-- Story Media is optional -->
				<div class="media">
					
					<div class="group">
						
						<!-- Story Image is required -->
						<img src="_demo/resources/images/standard-placeholder.png" width="229" height="153" alt="Lorem ipsum">
						
					</div>
					
				</div>
				
				<!-- Story Content is required -->
				<div class="content">
					
					<!-- Story Section is optional -->
					<h4>Lorem ipsum</h4>
					
					<div class="group">
						
						<!-- Story Indicators are optional -->
						<!-- START Component: Indicators -->
						<div class="indicators">
							
							<!-- Indicators Heading for accessibility -->
							<h5>Additional content available:</h5>
							
							<!-- Indicators List is required -->
							<ul>
								<!-- All Indicators are optional -->
								<li class="gallery">Gallery available</li>
								<li class="video">Video availble</li>
								<li class="comments">Comments accepted</li>
								<!-- Further Indicators can be added -->
							</ul>
							
						</div>
						<!-- END Component: Indicators -->
						
						<!-- Story Headline is required -->
						<h3>Lorem ipsum dolor sit amet</h3>
						
						<!-- Story Write-off is required -->
						<p><time datetime="[date/time]">[timestamp]</time> Lorem ipsum dolor sit amet, consectetur adipiscing elit. In congue fermentum aliquet. Aliquam eget ante quam, eu commodo orci. Donec varius laoreet sodales. Cras ut magna eu turpis adipiscing aliquam. Donec sed dolor et dui varius auctor.</p>
						
					</div>
					
				</div>
				
			</a>
			
		</article>
		<!-- END Component: Standard Story (Image Media) -->
		
		
		<!-- Standard Item (Portrait & Landscape) -->
		<!-- START Component: Standard Story (Image Media) -->
		<article class="story story-standard">
			
			<!-- Story Link is optional -->
			<a href="javascript:;">
				
				<!-- Story Media is optional -->
				<div class="media">
					
					<div class="group">
						
						<!-- Story Image is required -->
						<img src="_demo/resources/images/standard-placeholder.png" width="229" height="153" alt="Lorem ipsum">
						
					</div>
					
				</div>
				
				<!-- Story Content is required -->
				<div class="content">
					
					<!-- Story Section is optional -->
					<h4>Lorem ipsum</h4>
					
					<div class="group">
						
						<!-- Story Indicators are optional -->
						<!-- START Component: Indicators -->
						<div class="indicators">
							
							<!-- Indicators Heading for accessibility -->
							<h5>Additional content available:</h5>
							
							<!-- Indicators List is required -->
							<ul>
								<!-- All Indicators are optional -->
								<li class="gallery">Gallery available</li>
								<li class="video">Video availble</li>
								<li class="comments">Comments accepted</li>
								<!-- Further Indicators can be added -->
							</ul>
							
						</div>
						<!-- END Component: Indicators -->
						
						<!-- Story Headline is required -->
						<h3>Lorem ipsum dolor sit amet</h3>
						
						<!-- Story Write-off is required -->
						<p><time datetime="[date/time]">[timestamp]</time> Lorem ipsum dolor sit amet, consectetur adipiscing elit. In congue fermentum aliquet. Aliquam eget ante quam, eu commodo orci. Donec varius laoreet sodales. Cras ut magna eu turpis adipiscing aliquam. Donec sed dolor et dui varius auctor.</p>
						
					</div>
					
				</div>
				
			</a>
			
		</article>
		<!-- END Component: Standard Story (Image Media) -->
		
		
		<!-- Standard Item (Portrait & Landscape) -->
		<!-- START Component: Standard Story (Image Media) -->
		<article class="story story-standard">
			
			<!-- Story Link is optional -->
			<a href="javascript:;">
				
				<!-- Story Media is optional -->
				<div class="media">
					
					<div class="group">
						
						<!-- Story Image is required -->
						<img src="_demo/resources/images/standard-placeholder.png" width="229" height="153" alt="Lorem ipsum">
						
					</div>
					
				</div>
				
				<!-- Story Content is required -->
				<div class="content">
					
					<!-- Story Section is optional -->
					<h4>Lorem ipsum</h4>
					
					<div class="group">
						
						<!-- Story Indicators are optional -->
						<!-- START Component: Indicators -->
						<div class="indicators">
							
							<!-- Indicators Heading for accessibility -->
							<h5>Additional content available:</h5>
							
							<!-- Indicators List is required -->
							<ul>
								<!-- All Indicators are optional -->
								<li class="gallery">Gallery available</li>
								<li class="video">Video availble</li>
								<li class="comments">Comments accepted</li>
								<!-- Further Indicators can be added -->
							</ul>
							
						</div>
						<!-- END Component: Indicators -->
						
						<!-- Story Headline is required -->
						<h3>Lorem ipsum dolor sit amet</h3>
						
						<!-- Story Write-off is required -->
						<p><time datetime="[date/time]">[timestamp]</time> Lorem ipsum dolor sit amet, consectetur adipiscing elit. In congue fermentum aliquet. Aliquam eget ante quam, eu commodo orci. Donec varius laoreet sodales. Cras ut magna eu turpis adipiscing aliquam. Donec sed dolor et dui varius auctor.</p>
						
					</div>
					
				</div>
				
			</a>
			
		</article>
		<!-- END Component: Standard Story (Image Media) -->
		
		
		<!-- Standard Item (Portrait & Landscape) -->
		<!-- START Component: Standard Story (Image Media) -->
		<article class="story story-standard">
			
			<!-- Story Link is optional -->
			<a href="javascript:;">
				
				<!-- Story Media is optional -->
				<div class="media">
					
					<div class="group">
						
						<!-- Story Image is required -->
						<img src="_demo/resources/images/standard-placeholder.png" width="229" height="153" alt="Lorem ipsum">
						
					</div>
					
				</div>
				
				<!-- Story Content is required -->
				<div class="content">
					
					<!-- Story Section is optional -->
					<h4>Lorem ipsum</h4>
					
					<div class="group">
						
						<!-- Story Indicators are optional -->
						<!-- START Component: Indicators -->
						<div class="indicators">
							
							<!-- Indicators Heading for accessibility -->
							<h5>Additional content available:</h5>
							
							<!-- Indicators List is required -->
							<ul>
								<!-- All Indicators are optional -->
								<li class="gallery">Gallery available</li>
								<li class="video">Video availble</li>
								<li class="comments">Comments accepted</li>
								<!-- Further Indicators can be added -->
							</ul>
							
						</div>
						<!-- END Component: Indicators -->
						
						<!-- Story Headline is required -->
						<h3>Lorem ipsum dolor sit amet</h3>
						
						<!-- Story Write-off is required -->
						<p><time datetime="[date/time]">[timestamp]</time> Lorem ipsum dolor sit amet, consectetur adipiscing elit. In congue fermentum aliquet. Aliquam eget ante quam, eu commodo orci. Donec varius laoreet sodales. Cras ut magna eu turpis adipiscing aliquam. Donec sed dolor et dui varius auctor.</p>
						
					</div>
					
				</div>
				
			</a>
			
		</article>
		<!-- END Component: Standard Story (Image Media) -->
		
		
		<!-- Standard Item (Portrait) / Brief Item (Landscape) -->
		<!-- START Component: Standard / Brief Story (Image Media) -->
		<article class="story story-standard-portrait story-brief-landscape">
			
			<!-- Story Link is optional -->
			<a href="javascript:;">
				
				<!-- Story Media is optional -->
				<div class="media">
					
					<div class="group">
						
						<!-- Story Image is required -->
						<img src="_demo/resources/images/standard-placeholder.png" width="229" height="153" alt="Lorem ipsum">
						
					</div>
					
				</div>
				
				<!-- Story Content is required -->
				<div class="content">
					
					<!-- Story Section is optional -->
					<h4>Lorem ipsum</h4>
					
					<div class="group">
						
						<!-- Story Indicators are optional -->
						<!-- START Component: Indicators -->
						<div class="indicators">
							
							<!-- Indicators Heading for accessibility -->
							<h5>Additional content available:</h5>
							
							<!-- Indicators List is required -->
							<ul>
								<!-- All Indicators are optional -->
								<li class="gallery">Gallery available</li>
								<li class="video">Video availble</li>
								<li class="comments">Comments accepted</li>
								<!-- Further Indicators can be added -->
							</ul>
							
						</div>
						<!-- END Component: Indicators -->
						
						<!-- Story Headline is required -->
						<h3>Lorem ipsum dolor sit amet</h3>
						
						<!-- Story Write-off is required -->
						<p><time datetime="[date/time]">[timestamp]</time> Lorem ipsum dolor sit amet, consectetur adipiscing elit. In congue fermentum aliquet. Aliquam eget ante quam, eu commodo orci. Donec varius laoreet sodales. Cras ut magna eu turpis adipiscing aliquam. Donec sed dolor et dui varius auctor.</p>
						
					</div>
					
				</div>
				
			</a>
			
		</article>
		<!-- END Component: Standard / Brief Story (Image Media) -->
		
		
		<!-- Standard Item (Portrait) / Brief Item (Landscape) -->
		<!-- START Component: Standard / Brief Story (Image Media) -->
		<article class="story story-standard-portrait story-brief-landscape">
			
			<!-- Story Link is optional -->
			<a href="javascript:;">
				
				<!-- Story Media is optional -->
				<div class="media">
					
					<div class="group">
						
						<!-- Story Image is required -->
						<img src="_demo/resources/images/standard-placeholder.png" width="229" height="153" alt="Lorem ipsum">
						
					</div>
					
				</div>
				
				<!-- Story Content is required -->
				<div class="content">
					
					<!-- Story Section is optional -->
					<h4>Lorem ipsum</h4>
					
					<div class="group">
						
						<!-- Story Indicators are optional -->
						<!-- START Component: Indicators -->
						<div class="indicators">
							
							<!-- Indicators Heading for accessibility -->
							<h5>Additional content available:</h5>
							
							<!-- Indicators List is required -->
							<ul>
								<!-- All Indicators are optional -->
								<li class="gallery">Gallery available</li>
								<li class="video">Video availble</li>
								<li class="comments">Comments accepted</li>
								<!-- Further Indicators can be added -->
							</ul>
							
						</div>
						<!-- END Component: Indicators -->
						
						<!-- Story Headline is required -->
						<h3>Lorem ipsum dolor sit amet</h3>
						
						<!-- Story Write-off is required -->
						<p><time datetime="[date/time]">[timestamp]</time> Lorem ipsum dolor sit amet, consectetur adipiscing elit. In congue fermentum aliquet. Aliquam eget ante quam, eu commodo orci. Donec varius laoreet sodales. Cras ut magna eu turpis adipiscing aliquam. Donec sed dolor et dui varius auctor.</p>
						
					</div>
					
				</div>
				
			</a>
			
		</article>
		<!-- END Component: Standard / Brief Story (Image Media) -->
		
	</body> 
</html>