<?php include "includes/template/variables.php" ?>

<!DOCTYPE html>
<html lang="en"> 
	<head>
		<?php include "includes/template/meta.php" ?>

		<title>Hero Media Index</title>

        <?php include "includes/template/styles.php" ?>
	</head>
	<body class="gallery-content">
		
		<!--
		Portrait: 1 Hero Item, 8 Standard Items
		-------------------------
		|               |       |
		|               |   2   |
		|               |       |
		|       1       |-------|
		|               |       |
		|               |   3   |
		|               |       |
		|---------------|-------|
		|       |       |       |
		|   4   |   5   |   6   |
		|       |       |       |
		|-------|-------|-------|
		|       |       |       |
		|   7   |   8   |   9   |
		|       |       |       |
		-------------------------
		
		Landscape: 1 Hero Item , 8 Standard Items
		---------------------------------
		|               |       |       |
		|               |   2   |   3   |
		|               |       |       |
		|       1       |-------|-------|
		|               |       |       |
		|               |   4   |   5   |
		|               |       |       |
		|---------------|-------|-------|
		|       |       |       |       |
		|   6   |   7   |   8   |   9   |
		|       |       |       |       |
		---------------------------------
		-->
		
		
		<div class="wrap">
			<section class="index">
				<a href="javascript:;">
					<article class="today">
						<div class="clip-lead"> <!-- target height = 365 -->
							<img src="_demo/resources/images/hero-placeholder.png" width="479" height="377" alt="Lorem ipsum">
						</div>
						<!-- Story Headline is required -->
						<hgroup>Lorem ipsum dolor sit amet</hgroup>
						<!-- handle the duration of the video -->
						<aside class="count">0:42</aside>
					</article>
				</a>

				<a href="javascript:;">
					<article>
						<img src="_demo/resources/images/standard-placeholder.png" width="229" height="153" alt="Lorem ipsum">
						<!-- Story Headline is required -->
						<hgroup>Lorem ipsum dolor sit amet</hgroup>
					</article>
				</a>

				<a href="javascript:;">
					<article>
						<img src="_demo/resources/images/standard-placeholder.png" width="229" height="153" alt="Lorem ipsum">
						<!-- Story Headline is required -->
						<hgroup>Lorem ipsum dolor sit amet</hgroup>
						<!-- handle the duration of the video -->
						<aside class="count">0:42</aside>
					</article>
				</a>

				<a href="javascript:;">
					<article>
						<img src="_demo/resources/images/standard-placeholder.png" width="229" height="153" alt="Lorem ipsum">
						<!-- Story Headline is required -->
						<hgroup>Lorem ipsum dolor sit amet</hgroup>
					</article>
				</a>

				<a href="javascript:;">
					<article>
						<img src="_demo/resources/images/standard-placeholder.png" width="229" height="153" alt="Lorem ipsum">
						<!-- Story Headline is required -->
						<hgroup>Lorem ipsum dolor sit amet</hgroup>
						<!-- handle the duration of the video -->
						<aside class="count">0:42</aside>
					</article>
				</a>

				<a href="javascript:;">
					<article>
						<img src="_demo/resources/images/standard-placeholder.png" width="229" height="153" alt="Lorem ipsum">
						<!-- Story Headline is required -->
						<hgroup>Lorem ipsum dolor sit amet</hgroup>
					</article>
				</a>

				<a href="javascript:;">
					<article>
						<img src="_demo/resources/images/standard-placeholder.png" width="229" height="153" alt="Lorem ipsum">
						<!-- Story Headline is required -->
						<hgroup>Lorem ipsum dolor sit amet</hgroup>
						<!-- handle the duration of the video -->
						<aside class="count">0:42</aside>
					</article>
				</a>

				<a href="javascript:;">
					<article>
						<img src="_demo/resources/images/standard-placeholder.png" width="229" height="153" alt="Lorem ipsum">
						<!-- Story Headline is required -->
						<hgroup>Lorem ipsum dolor sit amet</hgroup>
					</article>
				</a>

				<a href="javascript:;">
					<article>
						<img src="_demo/resources/images/standard-placeholder.png" width="229" height="153" alt="Lorem ipsum">
						<!-- Story Headline is required -->
						<hgroup>Lorem ipsum dolor sit amet</hgroup>
						<!-- handle the duration of the video -->
						<aside class="count">0:42</aside>
					</article>
				</a>
			</section>
		</div>
		
	</body> 
</html>