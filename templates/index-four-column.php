<?php include "includes/template/variables.php" ?>

<!DOCTYPE html>
<html lang="en"> 
	<head>
		<?php include "includes/template/meta.php" ?>

		<title>Standard Media Index</title>

        <?php include "includes/template/styles.php" ?>
	</head>
	<body class="gallery-content">
		
		<!--
		Portrait: 12 Standard Items
		-------------------------
		|       |       |       |
		|   1   |   2   |   3   |
		|       |       |       |
		|-------|-------|-------|
		|       |       |       |
		|   4   |   5   |   6   |
		|       |       |       |
		|-------|-------|-------|
		|       |       |       |
		|   7   |   8   |   9   |
		|       |       |       |
		|-------|-------|-------|
		|       |       |       |
		|   10  |   11  |   12  |
		|       |       |       |
		-------------------------
		
		Landscape: 12 Standard Items
		---------------------------------
		|       |       |       |       |
		|   1   |   2   |   3   |   4   |
		|       |       |       |       |
		|-------|-------|-------|-------|
		|       |       |       |       |
		|   5   |   6   |   7   |   8   |
		|       |       |       |       |
		|-------|-------|-------|-------|
		|       |       |       |       |
		|   9   |   10  |   11  |   12  |
		|       |       |       |       |
		---------------------------------
		-->
		
		
		<div class="wrap">
			<section class="index">
				<a href="javascript:;">
					<article>
						<img src="_demo/resources/images/standard-placeholder.png" width="229" height="153" alt="Lorem ipsum">
						<!-- Story Headline is required -->
						<hgroup>Lorem ipsum dolor sit amet</hgroup>
					</article>
				</a>

				<a href="javascript:;">
					<article>
						<img src="_demo/resources/images/standard-placeholder.png" width="229" height="153" alt="Lorem ipsum">
						<!-- Story Headline is required -->
						<hgroup>Lorem ipsum dolor sit amet</hgroup>
						<!-- handle the duration of the video -->
						<aside class="count">0:42</aside>
					</article>
				</a>

				<a href="javascript:;">
					<article>
						<img src="_demo/resources/images/standard-placeholder.png" width="229" height="153" alt="Lorem ipsum">
						<!-- Story Headline is required -->
						<hgroup>Lorem ipsum dolor sit amet</hgroup>
					</article>
				</a>

				<a href="javascript:;">
					<article>
						<img src="_demo/resources/images/standard-placeholder.png" width="229" height="153" alt="Lorem ipsum">
						<!-- Story Headline is required -->
						<hgroup>Lorem ipsum dolor sit amet</hgroup>
						<!-- handle the duration of the video -->
						<aside class="count">0:42</aside>
					</article>
				</a>

				<a href="javascript:;">
					<article>
						<img src="_demo/resources/images/standard-placeholder.png" width="229" height="153" alt="Lorem ipsum">
						<!-- Story Headline is required -->
						<hgroup>Lorem ipsum dolor sit amet</hgroup>
					</article>
				</a>

				<a href="javascript:;">
					<article>
						<img src="_demo/resources/images/standard-placeholder.png" width="229" height="153" alt="Lorem ipsum">
						<!-- Story Headline is required -->
						<hgroup>Lorem ipsum dolor sit amet</hgroup>
						<!-- handle the duration of the video -->
						<aside class="count">0:42</aside>
					</article>
				</a>

				<a href="javascript:;">
					<article>
						<img src="_demo/resources/images/standard-placeholder.png" width="229" height="153" alt="Lorem ipsum">
						<!-- Story Headline is required -->
						<hgroup>Lorem ipsum dolor sit amet</hgroup>
					</article>
				</a>

				<a href="javascript:;">
					<article>
						<img src="_demo/resources/images/standard-placeholder.png" width="229" height="153" alt="Lorem ipsum">
						<!-- Story Headline is required -->
						<hgroup>Lorem ipsum dolor sit amet</hgroup>
						<!-- handle the duration of the video -->
						<aside class="count">0:42</aside>
					</article>
				</a>

				<a href="javascript:;">
					<article>
						<img src="_demo/resources/images/standard-placeholder.png" width="229" height="153" alt="Lorem ipsum">
						<!-- Story Headline is required -->
						<hgroup>Lorem ipsum dolor sit amet</hgroup>
					</article>
				</a>

				<a href="javascript:;">
					<article>
						<img src="_demo/resources/images/standard-placeholder.png" width="229" height="153" alt="Lorem ipsum">
						<!-- Story Headline is required -->
						<hgroup>Lorem ipsum dolor sit amet</hgroup>
						<!-- handle the duration of the video -->
						<aside class="count">0:42</aside>
					</article>
				</a>

				<a href="javascript:;">
					<article>
						<img src="_demo/resources/images/standard-placeholder.png" width="229" height="153" alt="Lorem ipsum">
						<!-- Story Headline is required -->
						<hgroup>Lorem ipsum dolor sit amet</hgroup>
					</article>
				</a>

				<a href="javascript:;">
					<article>
						<img src="_demo/resources/images/standard-placeholder.png" width="229" height="153" alt="Lorem ipsum">
						<!-- Story Headline is required -->
						<hgroup>Lorem ipsum dolor sit amet</hgroup>
						<!-- handle the duration of the video -->
						<aside class="count">0:42</aside>
					</article>
				</a>
			</section>
		</div>
		
	</body> 
</html>