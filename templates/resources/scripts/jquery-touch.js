/*!
 * Simple touch events
 *
 * Developed for Fairfax Media.
 */
 
(function() {
    
    function generateMouseEvent(target, type, touch, button) {
	var mouseEvent = document.createEvent("MouseEvent");
	mouseEvent.initMouseEvent(type, true, true, window, button, touch.screenX, touch.screenY, touch.clientX, touch.clientY, 
	    false, false, false, false, button, null);
	target.dispatchEvent(mouseEvent);
    }
    
    function calculateDelta(s, e) {
	return { 
	    x: e.x - s.x,
	    y: e.y - s.y,
	}
    }
    
    window.enableSwipe = function(element, direction) {
	var start;
	var pagingEnabled = true;
	
	element.addEventListener("touchstart", function (e) {
	  pagingEnabled = true;
	    var touch = e.changedTouches[0];
	    start = { 
		x: touch.pageX,
		y: touch.pageY
	    };
	    
	    // Carousel needs both events
	    generateMouseEvent(e.target, 'mouseover', touch, 0);
	    generateMouseEvent(e.target, 'mousedown', touch, 0);
	}, true);
	    
	element.addEventListener("touchmove", function (e) {
	    var touch = e.changedTouches[0],
		d = calculateDelta(start, { 
		    x: touch.pageX,
		    y: touch.pageY
		});

	    // Determine which events to block
	    if (direction === 'vertical') {
		if (Math.abs(d.y) > Math.abs(d.x)) {
		  e.preventDefault();
		  } else {
		    pagingEnabled = false;
		  }
	    } else if (direction === 'horizontal') {
		if (Math.abs(d.x) > Math.abs(d.y)) {
		  e.preventDefault();
		} else {
		  pagingEnabled = false;
		}
	    } else { // Both
		e.preventDefault();
	    }
	    
	    if (pagingEnabled) {
	      generateMouseEvent(e.target, 'mousemove', touch, 0);		
	    }
	}, true);
	
	element.addEventListener("touchend", function (e) {
	    start = null;
	    var touch = e.changedTouches[0];
	    generateMouseEvent(e.target, 'mouseup', touch, 0);		
	}, true);
	    
	element.addEventListener("touchcancel", function() {
	    start = null;
	}, true);
    }
    
})();
