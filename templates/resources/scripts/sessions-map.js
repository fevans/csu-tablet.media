			// Custom google maps initialisation
			$(function(){
				//center the map here
				var secheltLoc = new google.maps.LatLng(-33.891033,151.251598);

				var myMapOptions = {
					 zoom: 11
					,center: secheltLoc
					,mapTypeId: google.maps.MapTypeId.ROADMAP
				};
				var theMap = new google.maps.Map(document.getElementById("cinema-map"), myMapOptions);

				//names
				var name=[];
				name.push('Hoyts Bondi Junction Cinemas');
				name.push('Fox Studios');

				//marker values
				var marker=[];
				var alphabet = ("abcdefghijklmnopqrstuvwxyz").split("");
				$.each(alphabet,function(index,data){
					marker.push(data);
				});
				
				// positioner
				var position=[];
				position.push(new google.maps.LatLng(-33.891033,151.251598));
				position.push(new google.maps.LatLng(-33.895177,151.225455));

				var contentString = [];
				contentString.push('<div class="map-content">'+
			      '<!--UI NOTE - If cinemas is favourited add class favourite to h3 --> <h3 class="favourite">Hoyts Bondi Junction Cinemas</h3>'+
			      '<ul><li>12:00pm</li><li>1:00pm</li><li>2:00pm</li><li>3:00pm</li><li>4:00pm</li><li>5:00pm</li></ul>'+
			      '<p><!--a class="cta" href="#">book tickets</a--></p>'+
			      '</div>');
				contentString.push('<div class="map-content">'+
			      '<h3>Fox Studios</h3>'+
			      '<ul><li>12:00pm</li><li>1:00pm</li><li>2:00pm</li><li>3:00pm</li><li>4:00pm</li><li>5:00pm</li></ul>'+
			      '<p><!--a class="cta" href="#">book tickets</a--></p>'+
			      '</div>');

				for (i = 0; i < position.length; i += 1) {
					createMarkers(position[i], contentString[i], name[i], marker[i]);
				}

				var ib = new InfoBox();


				function createMarkers(position,content,name,marker) {
					var image = 'resources/images/map-marker.png';
					var marker = new MarkerWithLabel({
						map: theMap,
						draggable: false,
						icon: image,
						position: position,
						visible: true,
						title: name,
						labelContent: marker,
				        labelAnchor: new google.maps.Point(3, 22),
				        labelClass: "marker-label",
				        labelInBackground: false
					});

					var myOptions = {
						 content: content
						,alignBottom: true
						,disableAutoPan: false
						,maxWidth: 318
						,pixelOffset: new google.maps.Size(4, -20)
						,zIndex: null
						,boxStyle: { 
							radius: 4
						 }
						,closeBoxMargin: "0"
						,closeBoxURL: ""
						,infoBoxClearance: new google.maps.Size(100, 100)
						,isHidden: false
						,pane: "floatPane"
						,enableEventPropagation: false
					};

					google.maps.event.addListener(marker, "click", function (e) {
						ib.setOptions(myOptions);
						ib.open(theMap, this);
					});

					google.maps.event.addListener(theMap, 'click', function() {
						if(ib){
							ib.close();
						}
					});
				}
			});