$(function(){
				//toggle for touch/click
				//detect touch device
				var is_touch_device; 
				if(is_touch_device = 'ontouchstart' in document.documentElement){
				 	var touch = true;
				}else{
					var touch = false;
				}
				

				if(touch){
					//jump to sessions
					$('.scrollToSessions').on({ 
						'touchstart' : function(){ 
							$('html, body').animate({
						        scrollTop: $('.session-times').offset().top
						    }, 1000);
					 	}
					});

					$('.switch').on({ 
						'touchstart' : function(){ 
							$(this).toggleClass('on');
					 		if(!$(this).hasClass('on')){
					 			$('.result.near').addClass('hide');
						 		$('.result.search').removeClass('hide');
						 		$('.find-cinema li a.near').parent().removeClass('selected');
						 		$('.find-cinema li a.search').parent().addClass('selected');
					 		}
					 		lservices = pollLocationServices();
							if(lservices){
								$('.lservices').find('p').addClass('hide');
							 	$('.lservices').next('h3').removeClass('hide').next('div.cinema-map').removeClass('hide');
							 	$('.lservices').parent().next('div').removeClass('hide').next('div').removeClass('hide');
							}
					 	}
					});

					$('.expand').on({ 
						'touchstart' : function(){ 
							$(this).find('span').toggleClass('hide');
					 		$(this).parent().next('.expand-info').toggleClass('hide');
					 	}
					});
					$('.favourite').on({ 
						'touchstart' : function(){ 
							$(this).find('span').toggleClass('hide');
					 	}
					});
					$('.tooltip').on({ 
						'touchstart' : function(){
							$('.result').each(function(){
					 			$(this).find('ul.cinema-time').each(function(){
						 			$(this).find('.tooltip span:not(.hide)').each(function(){
						 				$(this).toggleClass('hide');
						 			})
						 		})
					 		})
					 		$(this).find('span').toggleClass('hide');
					 	}
					});
					$('.tooltip button').on({ 
						'touchstart' : function(event){
							$(this).parent('span').addClass('hide');
					 		event.stopPropagation();
					 	}
					});

					//Navigation
					$('.find-cinema li a').on({ 
					 	'touchstart' : function(){
					 			var page = $(this).attr('class');
						 		$(this).parent().parent().find('li').each(function(){
						 			$(this).removeClass('selected');
						 		});
						 		$(this).parent().addClass('selected');
						 		switch(page){
						 			case ('near'):
						 				$('.result').addClass('hide');
							 			$('.result.near').removeClass('hide');
							 			lservices = pollLocationServices();
							 			if(!lservices){
							 				$('.lservices').find('p').removeClass('hide');
							 				$('.lservices').next('h3').addClass('hide').next('div.cinema-map').addClass('hide');
							 				$('.lservices').parent().next('div').addClass('hide').next('div').addClass('hide');
							 			}
							 			break;
						 			case ('search'):
						 				$('.result').addClass('hide');
						 				$('.result.search').removeClass('hide');
						 				break;
						 			case ('fav'):
						 				$('.result').addClass('hide');
						 				$('.fav.result').removeClass('hide');
						 				break;
						 		}
					 	} 
					});
					//Session nav
					$('.cinema-day li a').on({
						'touchstart' : function(){
							var currsec = $(this).attr('class');
							$(this).parent().parent().find('li a').each(function(){
								$(this).addClass('rahhhhh');
								$(this).removeClass('selected');
							})
							$(this).addClass('selected');
					 	}
					}); 
				}else{
					//jump to sessions
					$('.scrollToSessions').on({ 
						'click' : function(){ 
							$('html, body').animate({
						        scrollTop: $('.session-times').offset().top
						    }, 1000);
					 	}
					});
					$('.switch').on({ 
					 	'click' : function(){
					 		$(this).toggleClass('on');
					 		if(!$(this).hasClass('on')){
					 			$('.result.near').addClass('hide');
						 		$('.result.search').removeClass('hide');
						 		$('.find-cinema li a.near').parent().removeClass('selected');
						 		$('.find-cinema li a.search').parent().addClass('selected');
					 		}
					 		lservices = pollLocationServices();
							if(lservices){
								$('.lservices').find('p').addClass('hide');
							 	$('.lservices').next('h3').removeClass('hide').next('div.cinema-map').removeClass('hide');
							 	$('.lservices').parent().next('div').removeClass('hide').next('div').removeClass('hide');
							}
					 	} 
					});
					$('.expand').on({ 
					 	'click' : function(){
					 		$(this).find('span').toggleClass('hide');
					 		$(this).parent().next('.expand-info').toggleClass('hide');
					 	} 
					});
					$('.favourite').on({ 
					 	'click' : function(){
					 		$(this).find('span').toggleClass('hide');
					 	} 
					});
					$('.tooltip').on({ 
					 	'click' : function(){
					 		$('.result').each(function(){
					 			$(this).find('ul.cinema-time').each(function(){
						 			$(this).find('.tooltip span:not(.hide)').each(function(){
						 				$(this).toggleClass('hide');
						 			})
						 		})
					 		})
					 		$(this).find('span').toggleClass('hide');
					 	} 
					});
					$('.tooltip button').on({ 
					 	'click' : function(event){
					 		$(this).parent('span').addClass('hide');
					 		event.stopPropagation();
					 	} 
					});
					//Navigation
					$('.find-cinema li a').on({ 
					 	'click' : function(){
					 		var page = $(this).attr('class');
					 		$(this).parent().parent().find('li').each(function(){
					 			$(this).removeClass('selected');
					 		});
					 		$(this).parent().addClass('selected');
					 		switch(page){
					 			case ('near'):
					 				$('.result').addClass('hide');
						 			$('.result.near').removeClass('hide');
						 			lservices = pollLocationServices();
						 			if(!lservices){
						 				$('.lservices').find('p').removeClass('hide');
						 				$('.lservices').next('h3').addClass('hide').next('div.cinema-map').addClass('hide');
						 				$('.lservices').parent().next('div').addClass('hide').next('div').addClass('hide');
						 			}
						 			break;
					 			case ('search'):
					 				$('.result').addClass('hide');
					 				$('.result.search').removeClass('hide');
					 				break;
					 			case ('fav'):
					 				$('.result').addClass('hide');
					 				$('.fav.result').removeClass('hide');
					 				break;
					 		}
					 		
					 	} 
					});
					//session nav
					$('.cinema-day li a').on({
						'click': function(){
							var currsec = $(this).attr('class');
							$(this).parent().parent().find('li a').each(function(){
								$(this).removeClass('selected');
							})
							$(this).addClass('selected');
						}
					}); 
				}
				//Simulate search
				$('.cinema-search').on({
					'keydown': function(e){
						 switch (e.which) {
						 	case 13: //enter
						 		$('.result.search >div').removeClass('hide');
						 		break;
						 }
					}
				})

				//check for location services
				function pollLocationServices(){
					if($('.switch').hasClass('on')){
						return true;
					}else{
						return false;
					}
				}
			});