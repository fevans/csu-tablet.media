(function() {
window.TabletTracking = {
    trackEvent: function(eventCode, identifier) {
        window.location.hash = 'tracking|' + eventCode + '|' + identifier;
    },
    trackVideoEvents: function(videoElementId, events) {
        var video = document.getElementById(videoElementId);
        var name = video.title;
        events = events || {
            'play': 'event19',
            'ended': 'event20',
        };

        for (var event in events) {
            (function (event, code) {
                video.addEventListener(event, function () {
                    TabletTracking.trackEvent(code, name);
                }, false);
            } (event, events[event]));
        }
    }
}
}());

